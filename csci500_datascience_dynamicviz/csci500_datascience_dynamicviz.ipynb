{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# CSCI B500 - Basics of **Dynamic Visualizations** for Data Science\n",
    "* In this unit, we make things “come alive” with _dynamic visualizations_. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Law of Large Numbers\n",
    "* For a six-sided die, each value 1 through 6 should occur one-sixth of the time, so the probability of any one of these values occurring is 1/6th or about 16.667%.  \n",
    "* In the dynamic visualization, the more rolls we perform, the closer each die value’s percentage of the total rolls gets to 16.667% and the heights of the bars gradually become about the same. \n",
    "* This is a manifestation of the _law of large numbers_. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How Dynamic Visualization Works \n",
    "* The Matplotlib **`animation`** module’s **`FuncAnimation`** function, updates a visualization _dynamically_. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Animation Frames\n",
    "* `FuncAnimation` drives a **frame-by-frame animation**. \n",
    "* Each **animation frame** specifies what to change during one plot update. \n",
    "* Stringing together many updates over time creates an animation. \n",
    "* This example displays an animation frame every 33 milliseconds—yielding approximately 30 (1000 / 33) frames-per-second. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running `dierolling_dynamicviz.py` from the command line\n",
    "1. Access the command line in Jupyter with **File > New > Terminal** (or just launch an Anaconda prompt separately)\n",
    "2. If necessary, use `cd` to navigate to the folder that contains `dierolling_dynamicviz.py`\n",
    "3. Execute\n",
    "\n",
    ">```\n",
    "ipython dierolling_dynamicviz.py 6000 1\n",
    "```\n",
    "\n",
    ">* 6000 is the number of animation frames to display.\n",
    ">* 1 is the number of die rolls to summarize in each animation frame.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* To see the law of large numbers in action, **increase the execution speed** by rolling the die more times per animation frame: \n",
    "\n",
    "```python\n",
    "ipython dierolling_dynamicviz.py 10000 600\n",
    "```\n",
    "\n",
    "*  In this case, `FuncAnimation` perform 10,000 updates, with 600 rolls-per-frame, for a total of 6,000,000 rolls. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sample Executions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Screen capture showing the graph after 64 of 6000 total die rolls](dynamicviz_images/Animation_01.png \"Screen capture showing the graph after 64 of 6000 total die rolls\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Screen capture showing the graph after 604 of the 6000 total die rolls](dynamicviz_images/Animation_02.png \"Screen capture showing the graph after 604 of 6000 total die rolls\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Screen capture showing the graph after 7200 of the 6,000,000 total die rolls](dynamicviz_images/Animation_03.png \"Screen capture showing the graph after 7200 of the 6,000,000 total die rolls\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Screen capture showing the graph after 166,200 of the 6,000,000 total die rolls](dynamicviz_images/Animation_04.png \"Screen capture showing the graph after 166,200 of the 6,000,000 total die rolls\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementing a Dynamic Visualization "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Importing the Matplotlib `animation` Module\n",
    "* We focus primarily on the new features used in this example. \n",
    "* We import the Matplotlib `animation` module to access `FuncAnimation`.\n",
    "\n",
    "```python \n",
    "# dierolling_dynamicviz.py\n",
    "\"\"\"Dynamically graphing frequencies of die rolls.\"\"\"\n",
    "from matplotlib import animation\n",
    "import matplotlib.pyplot as plt\n",
    "import random \n",
    "import seaborn as sns\n",
    "import sys\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function `update`\n",
    "```python\n",
    "def update(frame_number, rolls, faces, frequencies):\n",
    "    \"\"\"Configures bar plot contents for each animation frame.\"\"\"\n",
    "\n",
    "```\n",
    "* `FuncAnimation` calls the `update` function once per animation frame. \n",
    ">* See more about `FuncAnimation` here: https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.animation.FuncAnimation.html#matplotlib.animation.FuncAnimation\n",
    "* This function must receive at least one argument. \n",
    "* Parameters:\n",
    "    * `frame_number`—The next value from `FuncAnimation`’s `frames` argument, which we’ll discuss momentarily. Though `FuncAnimation` requires the `update` function to have this parameter, we do not use it in this `update` function.\n",
    "    * `rolls`—The number of die rolls per animation frame.\n",
    "    * `faces`—The die face values used as labels along the graph’s _x_-axis.\n",
    "    * `frequencies`—The list in which we summarize the die frequencies.\n",
    "\n",
    "We discuss the rest of the function’s body below.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function `update`: Rolling the Die and Updating the `frequencies` List\n",
    "* Roll the die `rolls` times and increment the appropriate `frequencies` element for each roll. \n",
    "> _(**Question:** when determining the index of `frequencies` whose element will be incremented, why call randrange(1, 7) and subtract 1? What purpose does this serve?)_\n",
    "\n",
    "```python\n",
    "    # roll die and update frequencies\n",
    "    for i in range(rolls):\n",
    "        frequencies[random.randrange(1, 7) - 1] += 1 \n",
    "```"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Question's answer: We get it as a randrange(1,7) because there are 6 \"faces\" on our die, so we want it to be one of the possible values. However, indexing in Python starts at 0. Each die face's index is going to mapped to its own corresponding value minus 1. \"1\" is stored in \"0\", \"2\" is stored in \"1\", etc.. It then increments by 1 because if that number came up, only that value's frequency should increase.\n",
    "This results in a list where the count of how many times each face appeared at stored in the die face's corresponding list index location."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function `update`: Configuring the Bar Plot and Text \n",
    "* The `matplotlib.pyplot` module’s **`cla`** (clear axes) function removes the existing bar plot elements before drawing new ones for the current animation frame. \n",
    "* We discussed the other code below when we previously discussed static visualization.\n",
    "\n",
    "```python\n",
    "    # reconfigure plot for updated die frequencies\n",
    "    plt.cla()  # clear old contents contents of current Figure\n",
    "    axes = sns.barplot(faces, frequencies, palette='bright')  # new bars\n",
    "    axes.set_title(f'Die Frequencies for {sum(frequencies):,} Rolls')\n",
    "    axes.set(xlabel='Die Value', ylabel='Frequency')  \n",
    "    axes.set_ylim(top=max(frequencies) * 1.10)  # scale y-axis by 10%\n",
    "\n",
    "    # display frequency & percentage above each patch (bar)\n",
    "    for bar, frequency in zip(axes.patches, frequencies):\n",
    "        text_x = bar.get_x() + bar.get_width() / 2.0  \n",
    "        text_y = bar.get_height() \n",
    "        text = f'{frequency:,}\\n{frequency / sum(frequencies):.3%}'\n",
    "        axes.text(text_x, text_y, text, ha='center', va='bottom')\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variables Used to Configure the Graph and Maintain State\n",
    "* The `sys` module’s `argv` list contains the script’s command-line arguments. \n",
    "* The `matplotlib.pyplot` module’s **`figure`** function gets a `Figure` object in which `FuncAnimation` displays the animation &mdash; one of `FuncAnimation`’s required arguments. \n",
    "\n",
    "```python\n",
    "# read command-line arguments for number of frames and rolls per frame\n",
    "number_of_frames = int(sys.argv[1])  \n",
    "rolls_per_frame = int(sys.argv[2])  \n",
    "# note: the argument at 0 is the name of the file itself\n",
    "\n",
    "sns.set_style('whitegrid')  # white background with gray grid lines\n",
    "figure = plt.figure('Rolling a Six-Sided Die')  # Figure for animation\n",
    "values = list(range(1, 7))  # die faces for display on x-axis\n",
    "frequencies = [0] * 6  # six-element list of die frequencies\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Calling the `animation` Module’s `FuncAnimation` Function\n",
    "* `FuncAnimation` returns an object representing the animation. \n",
    "* Though this is not used explicitly, you _must_ store the reference to the animation in a variable (such as `die_animation` in the example below); otherwise, Python immediately terminates the animation and returns its memory to the system. \n",
    "\n",
    "```python\n",
    "# configure and start animation that calls function update\n",
    "die_animation = animation.FuncAnimation(\n",
    "    figure, update, repeat=False, frames=number_of_frames, interval=33,\n",
    "    fargs=(rolls_per_frame, values, frequencies))\n",
    "\n",
    "plt.show()  # display window\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`FuncAnimation` has two required arguments:\n",
    "* `figure`—the `Figure` object in which to display the animation, and\n",
    "* `update`—the function to call once per animation frame.\n",
    "\n",
    "* Optional keyword arguments:\n",
    "    * **`repeat`**—If `True` (the default), when the animation completes it restarts from the beginning.\n",
    "    * **`frames`**—The total number of animation frames, which controls how many times `FunctAnimation` calls `update`. \n",
    "    * **`interval`**—The number of milliseconds between animation frames (the default is 200).\n",
    "    * **`fargs`** (short for “function arguments”)—A tuple of other arguments to pass to the function you specified in `FuncAnimation`’s second argument. \n",
    "* [`FuncAnimation`’s other optional arguments](https://matplotlib.org/api/_as_gen/matplotlib.animation.FuncAnimation.html)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------                 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYUAAAESCAYAAAASQMmzAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDMuMC4zLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvnQurowAAIABJREFUeJzt3X1YFPX+//HnciMa4F3elyl6FBUz9ZAeE7WjmTeIaEoKHbvB8ibvUDuKmmApaKJpQXqyG+t4r4hJv6Nl2lGzKMuvYSGZeZOpKBmogLos7P7+8HJOhDdrsKzi63FdXBc7+5nZ9yzDvnY+M58Zk81msyEiIgK4OLsAERG5dSgURETEoFAQERGDQkFERAwKBRERMSgURETE4ObsAuT25uvrS9OmTXFx+d/3i5YtWxITE4Ovry8pKSns3buXlJQUXnzxxT/1GqtWrSInJ4dhw4aVVtkO8dprr9GgQQP69etn9zyffvopkZGR1K1b15i2YsUKvLy82L59O/Pnzyc/Px9fX19iY2Px8vKisLCQOXPm8Nlnn1FYWEh4eDihoaFFlnv+/HmGDBkCwIULFzh9+jQ+Pj4APPTQQ/j7+5fobyLlmE2kBJo2bWr77bffbvo5uWzevHm2xYsXF5v+22+/2f72t7/Zjhw5YrPZbLa5c+faoqOjbTabzbZ8+XLbs88+a7NYLLazZ8/aevToYUtNTb3ma3z55Ze2wMBAR5Qv5ZD2FMThkpKS+Pjjj3nzzTcZMmQILVq0YM+ePWRnZxMcHMzYsWM5fvw4Q4YMoVOnTqSmpmKz2YiKisLf35/4+Hiys7OJioqia9eu9O/fn5SUFDIyMggODiYiIgKAJUuWkJiYiKenJ/7+/mzbto1PP/20SC1Wq5XY2FhSU1PJy8vDZrMxa9Ys/vrXvxarOzExkaVLl+Li4kK1atV45ZVXqFu3LmvWrGHZsmW4uLhQo0YNpk+fjo+PD5GRkTRp0oShQ4dy//33M2zYMD7//HMyMzN59tlnCQsLK/Yae/fuxc3NjU2bNuHl5cX48eN58MEH2bVrF/fffz8NGzYEIDQ0lODgYKKjo9m6dSuPP/44bm5uVKlShcDAQJKTk2nVqtWf/pv4+fnx7bffkpWVxeOPP86ZM2fYvXs3Fy9eZOHChfj6+pKTk0NMTAw//vgjFouFDh06MGnSJNzc3Hj99df55JNPcHd3p1q1asyePZtatWrdxFYitwodU5ASe+qppwgODjZ+fvvtt+u2P3LkCKtWrWLDhg1s2rSJ//73vwCcPHmSBx98kI0bNzJx4kQiIiKwWCzF5r9w4QIrV65k9erVvPvuu/zyyy989tlnJCUlkZiYSFJSEnl5eVd97dTUVDIzM1mzZg2bNm2if//+vPXWW8Xa/fDDD8ybN4+3336bDz/8kK5du7J48WJSUlJ4++23+fe//01ycjJ9+vRh1KhR2P5wYYD8/HyqVavG6tWref3115k9ezZms7nY61StWpXBgwezceNGJkyYwOjRozl16hSnTp2iTp06Rrs6deqQm5tLXl4eGRkZRbqb6tSpw6lTp677nt/IiRMnWL16NXFxccTFxdGuXTuSkpLo1KkTy5cvByA2NhY/Pz+SkpL44IMPyM7OZunSpWRkZPD++++zfv16kpKS6NixI/v27StRPeI82lOQEnv//fepXr263e0HDRqEu7s77u7u9OzZk127dtGkSROqVKlCUFAQAF26dMHV1ZUDBw4Um79bt24A1K5dm7vvvptz586xY8cOevbsSeXKlQF44okn+PLLL4vN26ZNG6pUqcLq1av55Zdf+Oqrr/D09CzWLiUlhYCAAOPD9+mnnwZg7ty59O7d21jfxx57jJiYGI4fP37NOv38/MjPz+fChQt4eHgUaZOQkGD87u/vT5s2bfj888+xWq2YTKZiy3RxccFmsxV5zmazFTmm82d0794dgPr16wPQqVMnAO677z52794NwPbt2/nuu+9ITEwE4NKlS8Dlv0OzZs3o378/nTt3pnPnznTo0KFE9YjzKBSkzLm5/W+z+/0Hmqura5F2Vqu12DSgyAeryWTCZrPh5uZW5Nv61eaDyx9sMTExPPPMM3Tr1o1GjRqRnJxcrJ2rq2uRD95Lly5x4sQJrFZrsbY2m42CgoJr1nllOX/cmzh//jwrV65k+PDhRdq4ublRt25dUlNTjbanT5+mSpUq3HXXXdStW5fMzEzjuczMzCJ7FX9GhQoVijx2d3cv1sZqtfLaa6/RuHFjo36TyYSLiwvLly/nu+++IyUlhdjYWDp16sSkSZNKVJM4h7qPpMwlJydjtVo5d+4cmzdvpmvXrgBkZWWxc+dO4PJZOe7u7jRt2tSuZXbp0oUtW7aQk5MDYHyb/aPPP/+cv//974SFhdGyZUu2bt1KYWFhsXbt27cnJSXF+PC90rXSqVMnNm3aRFZWFgDr16+natWqNGjQ4ObeBMDT05MVK1awZcsWAPbv38++ffvo1KkTAQEBpKamcvToUeP1r+x5dOvWjfXr11NQUMD58+f5z3/+wyOPPHLTr3+zAgICeO+997DZbOTn5zNy5EiWL1/ODz/8QJ8+fWjcuDHDhw/n6aef5rvvvnN4PeIY2lOQMnfp0iUGDhxIXl4eYWFhdOjQgePHj+Ph4cHGjRuZN28eFStW5I033rjmN/4/6tChA48//jiDBg2iYsWKNGnShEqVKhVrN3jwYCZOnEhQUBAFBQV07NiRLVu2YLVai3TB+Pr68s9//pNnn30WgJo1axIbG0vt2rV5+umneeqpp7BarVSvXp0333zzT3XfuLq6smjRImbNmkV8fDyurq4sWLDA6JqaPXs2Y8eOxWKxcN999/HKK68Alw86Hzt2jODgYCwWC4MGDaJdu3Y3/fo3a9q0acTExBAUFITFYuGhhx7i2Wefxd3dnV69ejFgwADuuusuKlasqFNdb2Mm2x/3aUUcaMiQITzxxBP07NmzyPTjx48TFBTE3r17/9Ryv/vuO/bu3cuTTz4JwNKlS0lNTWXhwoUlrlnkTqI9BSkXfHx8eOutt1i7di0mk4m6desyc+ZMZ5clctvRnoKIiBh0oFlERAzqPhIRp0hISGDz5s3A5bPHJk2axJQpU9izZ49xksDo0aPp3r07e/fuZfbs2eTl5eHr68ucOXOKnUYrpeO26z769ttviw0AEpHbS2pqKqtWrWLmzJmYTCZeeuklAgMDWblyJTNmzCgyGPLChQuMGjWK6OhoGjZsyPz582nRogW9evVy4hrcfsxmM61bt75hu9tuT8HDw4PmzZs7uwwRKQE3NzeaNWtmXK+pVatWuLi4kJWVxdKlSzl9+jTdu3dn9OjRfPLJJ/j7+xshMHfuXAoLC6lZs6YzV+G2k56eble72y4UROT216RJE+P3o0ePsnnzZlasWMHu3buJjo7G29ub4cOHk5iYyNmzZ7nrrrsYP348hw8fpm3btkRGRjqx+vJNB5pFxGkOHjxIeHg4kyZNolGjRrzxxhvUqlWLSpUqMWTIEHbs2EFhYSG7du1iwoQJJCUlcfHiRZYsWeLs0ssthYKIOMWePXt4+umnmThxIv379+fAgQN8/PHHxvNXrgNVo0YNHnjgAerXr4+rqyu9evXSVVgdSKEgImUuIyODUaNGMW/ePAIDA4HLIRAbG8u5c+ewWCysWbOG7t27ExAQQFpaGhkZGQD897//xc/Pz5nll2s6piAiZe6dd97BbDYzZ84cY9rgwYMZNmwYoaGhFBQU8Oijj9KnTx8AXn75ZUaMGIHZbKZ58+ZMnjzZWaWXe7fdKanp6ek6+0hE5CbZ+9mp7iMRETEoFMq5hIQEAgMDCQwMZO7cuUWeW758OUOGDCk2z/79+2nZsmVZlSgitxCFQjn2xRdfsGvXLjZs2MAHH3xAWloan3zyCQA//fTTVU/ru3jxIjNnzrzqvZFFpPxTKJRjNWvWJDIykgoVKuDu7k7jxo05efIk+fn5REVFMXbs2GLzzJkzh6eeesoJ1crtbsiQIQQGBhIcHExwcLBxO9Hc3Fz69Olz1ftYA2zdupXg4GD69u3L888/z7lz5wA4efKkce+NkSNHkpeXB1y+DeiwYcPo1asXTzzxBL/++isA33zzDT169KBv374cOXIEgLy8PEaMGOHoVS9XFArlWJMmTYxrnVwZNdqlSxfmz5/PgAEDjJu0X7Ft2zYuXbpU7AY4Ijdis9k4evQoGzduNH4eeOABUlNTCQ0NNW4r+ke5ubnMmDGDJUuWkJycjK+vL/Hx8QC89NJLhIWF8dFHH9GyZUsWLVoEwMKFC/H392fz5s2EhIQQExMDXL6xUlxcHCNHjmT16tUAvP3224SHhzv+DShHFAp3gN+PGj1x4gQZGRkMGDCgSJtff/2VxYsXM336dCdVKbezw4cPAxAeHk7fvn1Zvnw5AGvXriU6OppatWpddT6LxUJ0dDS1a9cGLt8GNSMjA4vFwtdff02PHj0AeOyxx/joo48A2L59O0FBQQD06dOHnTt3YrFYcHd358KFC+Tl5eHu7k5mZiZHjx4tk1uVlicap1DO7dmzh7FjxzJ16lQCAwOZMmUKBw8eJDg4mAsXLnDmzBkiIiLo2LEjZ8+e5YknnjDmDQ4OZsWKFXh5eTlxDeR2cP78eTp06MD06dOxWCw8+eST+Pj4GN/ir6VatWp0794duHzv7iVLljBkyBCys7Px8vLCze3yR1TNmjU5ffo0AJmZmcbF8Nzc3PDy8iIrK4vhw4czffp0KlWqxLx580hISGDkyJEOXOvySaFQjl0ZNbpgwQI6dOgAXL4Z/BVfffUVCQkJxn2MQ0JCjOd8fX3ZuHFj2RYst602bdrQpk0b4/HAgQPZsWMHHTt2tGv+nJwcRo0aRbNmzejfvz+nT5/GZDIVafPHx1fYbDZcXFxo3rw5iYmJABw6dAiAqlWrMmbMGCwWC5MnT8bHx+fPrN4dRd1H5djvR41eOfi3atUqZ5cl5dA333xDSkqK8fjKdYvskZmZSVhYGL6+vsaeRfXq1cnJyaGwsBC43L15pQuqVq1anDlzBoCCggLy8vKoWrVqkWXGx8fz/PPP8+9//5tHH32U4cOHk5CQUOL1vBNoT6Ece/HFF3nxxRev+Xz79u1p3779VZ87cOCAo8qScignJ4fXX3+d1atXY7FY2LBhAy+99NIN5yssLGTEiBH06tWL559/3pju7u6Ov78/mzZtIigoiA8++IDOnTsDl+/S9sEHHzBixAg2bdqEv78/7u7uxrxff/019evXp06dOlgsFlxdXXFxccFsNpf+ipdDCgURKbG///3vpKam0q9fP6xWK2FhYUW6k/7oueeeY+zYsZw6dYr9+/dTWFhoXCG1ZcuWxMTEEB0dTWRkJIsXL6Zu3bq8+uqrAIwbN47IyEgCAwPx9vZm3rx5RZa9ZMkSo21ISAjjxo3DarUSGxvroLUvX3TtIxGRO4CufVSOvfLKK8adp9LS0hgwYAB9+/Zl+PDhnD9/vlj7/Px8Jk6cSFBQEMHBwXzxxRfGc++++y49e/akR48ebNmyxZj+4Ycf0rt3bx599FFWrFhhTJ8wYQLdunVj/vz5xrQlS5awY8cOR6zqHcdqUReHFFeW24W6j24zKSkpbNiwgYcffhiAmJgYxo4dS5cuXZgzZw7vvPMO48ePLzLPxo0bsVqtfPjhhxw4cIDnnnuOnTt3sm/fPpKTk9m4cSO5ubkMGjSIdu3aYTabWbBgAUlJSVSoUIHBgwfTvn17CgoKyMnJYdu2bQQFBTFs2DAKCwvZt28fw4YNc8K7Uf64uHtwNEpnyEhRDV8+UmavVeqhYLFYmDp1KidOnCA/P5+RI0dSp04dRowYQcOGDQEIDQ2ld+/eJCQksH37dtzc3Jg6dapxE2+5urNnz7JgwQJGjBjBDz/8AIDVajWG/1+8eJEqVaoUm89qtXLx4kUKCwu5ePEiFStWBGDnzp10794dDw8PPDw8aNeuHdu3b8dms/G3v/3NOKOjR48efPTRR/Tq1Quz2cylS5eMA3hvvPGGAkGkHCn1UEhOTqZq1arExcWRnZ1N//79GTVqFM8880yR4eZpaWns3r2bdevWkZGRwZgxY1i/fn1pl1OuREVFMX78eOMOVACRkZGEh4cTGxtLpUqVWLt2bbH5+vfvz4YNG+jUqRPnz583DsJlZmZy//33G+1q1qzJqVOnMJlMxuAguHwK4L59+2jcuDHNmjXjscceIzQ0lN9++43s7GyFuUg5UuqhcKV/+gpXV1e+//57jhw5wrZt22jQoAFTp05lz549BAQEYDKZqFevHoWFhWRlZVG9evXSLqlcWLduHXXr1qVDhw4kJSUBl0eATps2jffee49WrVqxdOlSJk+eXOzqpwkJCbRu3ZpVq1Zx9OhRnn76afz8/LBarcVex8XFhcLCwiIDhWw2m/F46tSpxvTJkyczatQoVqxYwaeffspf//rXIqcVisjtp9RDwdPTE7h8oauxY8cSERFBfn4+ISEhtGzZksWLF/PGG2/g7e1dZMCJp6cnOTk5NwwFs9lMenp6aZd9y1u3bh3Z2dls376d3NxcLl26xMGDB7HZbLi7u5Oenk7r1q1ZsGBBsfdn06ZNvPDCC0aXU6NGjdi8eTOurq7s37/faH/w4EFatmyJzWbj4MGDxvT9+/fj4uJSZLk//fQTBQUFnDlzhvfee4/XXnuNl19+maZNm3LPPfeU0btS/ujMOrmWsvrcc8iB5iuXVwgLCyMoKIjz589TuXJlALp3787MmTPp1q2b0RcOly9x6+3tfcNle3h43JH/OL/vFkpKSmL37t1MmTKFXr164eHhQaNGjfjwww954IEHir0/DzzwAIcPH6Znz55kZWXx888/Ex0dzfnz54mKimLSpElcvHiRH374gejoaODycZ/atWtTqVIl9u7dy8yZM4ssNz4+nlmzZuHu7o6HhwctWrTAy8uLe++9l2bNmpXNmyJyBynp5569oVLqoXDmzBnCw8OJiooyrrczdOhQpk+fTqtWrUhJScHPz4+2bdsSFxfH0KFDOXXqFFarVV1HN6lKlSrMnj2biIgIbDYbd999tzFAZ9WqVWRmZjJu3DimTJnC9OnTCQwMxMXFhQkTJhgH/fv27cvAgQMpKChg7NixxtUqx48fz5NPPonFYmHgwIFFjhvs2LEDPz8/4+/VsWNHunbtir+/vwJB5DZX6oPXZs2axebNm2nUqJExLSIigri4ONzd3alRowYzZ87Ey8uL+Ph4du7cidVqZcqUKfj7+99w+Rq8JuWdTkmVPyqNU1Lt/ey8I0c0m/OteFTQuD35n1tpm1AoyB+VZSjckYPXPCq40LjfUWeXIbeQQx80dHYJIreEW+OrkYiI3BIUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYnAr7QVaLBamTp3KiRMnyM/PZ+TIkfzlL38hMjISk8lEkyZNiI6OxsXFhYSEBLZv346bmxtTp06lVatWpV2OiIjchFIPheTkZKpWrUpcXBzZ2dn079+fZs2aERERQfv27YmKimLbtm3Uq1eP3bt3s27dOjIyMhgzZgzr168v7XJEROQmlHoo9OzZkx49ehiPXV1dSUtLo127dgB07tyZzz//HB8fHwICAjCZTNSrV4/CwkKysrKoXr36dZdvNptJT08vUY3Nmzcv0fxSPpV0uyoN2jblWspq+yz1UPD09AQgNzeXsWPHEhERwSuvvILJZDKez8nJITc3l6pVqxaZLycn54ah4OHhoX8ccQhtV3IrK+n2aW+oOORAc0ZGBk8++STBwcEEBQXh4vK/l8nLy6Ny5cp4eXmRl5dXZLq3t7cjyhERETuVeiicOXOG8PBw/vnPfzJw4EAAWrRowVdffQXAzp078ff3p23btuzatQur1crJkyexWq033EsQERHHKvXuo3/961+cP3+eRYsWsWjRIgCmTZvGrFmzePXVV2nUqBE9evTA1dUVf39/Bg0ahNVqJSoqqrRLERGRm2Sy2Ww2ZxdxM9LT00ul77dxv6MlL0bKjUMfNHR2CYajUT7OLkFuMQ1fPlLiZdj72anBayIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGu0LhzJkzjq5DRERuAW72NBozZgzVq1dn4MCBdOnSBRcX7WCIiJRHdoXCqlWrOHToEImJiSxevJgOHTowcOBA6tev7+j6RESkDNn9lb9WrVrUr1+fihUr8uOPPxITE8Nrr73myNpERKSM2bWnMG7cOA4ePEjfvn2Ji4ujdu3aADz22GOMGzfOoQWKiEjZsSsUHn/8cVq3bo2npyeZmZnG9FWrVjmsMBERKXt2dR/t3buX+Ph4AGbNmsWSJUsA8PDwcFxlIiJS5uwKhU8//ZTIyEgAXn/9dT799FOHFiUiIs5hVyiYTCby8/MBsFgs2Gw2hxYlIiLOYdcxhcGDBxMUFETTpk05fPgwzz77rKPrEhERJ7ArFEJCQujWrRu//PIL9evXp3r16o6uS0REnMCuUEhPT2fNmjWYzWZj2uzZs687T2pqKvPmzWPZsmWkpaUxYsQIGjZsCEBoaCi9e/cmISGB7du34+bmxtSpU2nVqtWfXxMRESkxu0IhMjKSf/zjH9SpU8euhb711lskJydTqVIlAPbv388zzzxDeHi40SYtLY3du3ezbt06MjIyGDNmDOvXr/8TqyAiIqXFrlCoUaMGISEhdi/0vvvuIz4+nkmTJgHw/fffc+TIEbZt20aDBg2YOnUqe/bsISAgAJPJRL169SgsLCQrK0tdUyIiTmRXKNxzzz0sWbKE5s2bYzKZAAgICLhm+x49enD8+HHjcatWrQgJCaFly5YsXryYN954A29vb6pWrWq08fT0JCcn54ahYDabSU9Pt6fsa2revHmJ5pfyqaTbVWnQtinXUlbbp12hYLFYOHLkCEeOHDGmXS8U/qh79+5UrlzZ+H3mzJl069aNvLw8o01eXh7e3t43XJaHh4f+ccQhtF3Jrayk26e9oWJXKMyePZsjR45w7NgxfH19qVWr1k0VM3ToUKZPn06rVq1ISUnBz8+Ptm3bEhcXx9ChQzl16hRWq1VdRyIiTmZXKCxfvpxPPvmEc+fO0b9/f37++WeioqLsfpEZM2Ywc+ZM3N3dqVGjBjNnzsTLywt/f38GDRqE1Wq9qeWJiIhj2BUK//nPf1i5ciVPPvkkTz31FAMGDLjhPPfeey9r164FwM/Pj9WrVxdrM2bMGMaMGXOTJYuIiKPYdZmLK5e1uHKQuUKFCo6rSEREnMauPYU+ffrwxBNPcPLkSZ577jkeeeQRR9clIiJOYFco/OMf/6BDhw78+OOP+Pj40KxZM0fXJSIiTmBXKCQkJBi/Hzp0iK1btzJ69GiHFSUiIs5h94hmuHxsYf/+/VitVocWJSIizmH3pbN/T5fOFhEpn+wKhd+PZP7111/JyMhwWEEiIuI8doXC7weWeXh4GBe6ExGR8sWuUFi2bJmj6xARkVuAXaHQt29f8vLy8PDwMG60Y7PZMJlMbNu2zaEFiohI2bErFNq0aUO/fv1o06YNBw4c4J133mHWrFmOrk1ERMqYXaFw6NAh2rRpA4Cvry8ZGRm61IWISDlkVyh4e3uzcOFCWrVqxZ49e6hXr56j6xIRESew64J48+fPx8vLi88++4z69esTExPj6LpERMQJ7AoFDw8PqlSpQrVq1fDx8eH8+fOOrktERJzArlCIiori5MmTfP755+Tl5TF58mRH1yUiIk5gVygcO3aMcePGUaFCBbp27UpOTo6j6xIRESewKxQKCwvJysrCZDKRm5uLi4tds4mIyG3GrrOPxo8fT2hoKL/++iuDBg1i2rRpjq5LREScwK5QyMjI4OOPPyYrK4tq1aoZt+UUEZHyxa5+oLVr1wJQvXp1BYKISDlm155Cfn4+/fr1w8fHxzieMH/+fIcWJiIiZe+6obBo0SKef/55XnjhBU6fPk3t2rXLqi4REXGC63YfffnllwC0a9eOdevW0a5dO+NHRETKn+uGgs1mu+rvIiJSPl03FH5/UFkHmEVEyr/rHlNIS0tj8ODB2Gw2fvrpJ+N3k8nE6tWry6pGEREpI9cNheTk5LKqQ0REbgHXDYV77rmnrOoQEZFbgC5iJCIiBoWCiIgYHBYKqampDBkyBICff/6Z0NBQwsLCiI6Oxmq1ApCQkMDAgQMZPHgw+/btc1QpIiJiJ4eEwltvvcWLL76I2WwGYPbs2URERLBy5UpsNhvbtm0jLS2N3bt3s27dOl599VVeeuklR5QiIiI3wSGhcN999xEfH288TktLM0ZBd+7cmS+++II9e/YQEBCAyWSiXr16xj0bRETEeey6IN7N6tGjB8ePHzceXxnbAODp6UlOTg65ublUrVrVaHNlevXq1a+7bLPZTHp6eonqa968eYnml/KppNtVadC2KddSVtunQ0Lhj35/p7a8vDwqV66Ml5cXeXl5RaZ7e3vfcFkeHh76xxGH0HYlt7KSbp/2hkqZnH3UokULvvrqKwB27tyJv78/bdu2ZdeuXVitVk6ePInVar3hXoKIiDhWmewpTJ48menTp/Pqq6/SqFEjevTogaurK/7+/gwaNAir1UpUVFRZlCIiItdhst1mlz9NT08vld38xv2OlrwYKTcOfdDQ2SUYjkb5OLsEucU0fPlIiZdh72enBq+JiIhBoSAiIgaFgoiIGBQKIiJiUCiIiIhBoSAiIgaFgoiIGBQKIiJiUCiIiIhBoSAiIgaFgoiIGBQKIiJiUCiIiIhBoSAiIgaFgoiIGBQKIiJiUCiIiIhBoSAiIgaFgoiIGBQKIiJ3ZlbAAAAHNUlEQVRiUCiIiIhBoSAiIgaFgoiIGBQKIiJiUCiIiIhBoSAiIgaFgoiIGBQKIiJiUCiIiIhBoSAiIgaFgoiIGNzK8sX69euHt7c3APfeey+DBg0iJiYGV1dXAgICGD16dFmWIyIif1BmoWA2mwFYtmyZMS04OJj4+Hjq16/PsGHDSEtLw8/Pr6xKEhGRPyizUPjhhx+4ePEi4eHhFBQUMGbMGPLz87nvvvsACAgIICUl5YahYDabSU9PL1EtzZs3L9H8Uj6VdLsqDdo25VrKavsss1CoWLEiQ4cOJSQkhKNHj/Lcc89RuXJl43lPT09++eWXGy7Hw8ND/zjiENqu5FZW0u3T3lAps1Dw8fGhQYMGmEwmfHx88Pb25uzZs8bzeXl5RUJCRETKXpmdfZSYmMicOXMAOH36NBcvXuSuu+7i2LFj2Gw2du3ahb+/f1mVIyIiV1FmewoDBw5kypQphIaGYjKZiI2NxcXFhRdeeIHCwkICAgJ44IEHyqocERG5ijILhQoVKjB//vxi09euXVtWJYiIyA1o8JqIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYlAoiIiIQaEgIiIGhYKIiBgUCiIiYnBzdgFWq5UZM2Zw4MABKlSowKxZs2jQoIGzyxIRuSM5fU9h69at5Ofns2bNGiZOnMicOXOcXZKIyB3L6aGwZ88eOnXqBEDr1q35/vvvnVyRiMidy+ndR7m5uXh5eRmPXV1dKSgowM3t6qWZzWbS09NL/Lr/b3aJFyHlSGlsU6UmdJOzK5BbTGlsn2az2a52Tg8FLy8v8vLyjMdWq/WagQCX9yZERMQxnN591LZtW3bu3AnAt99+S9OmTZ1ckYjInctks9lszizgytlHP/74IzabjdjYWBo3buzMkkRE7lhODwUREbl1OL37SEREbh0KBRERMSgURETEoFAoh5KSkpg3b16pLGvVqlXEx8eXyrJErsZsNrNu3bprPj9+/Hjy8/OJjIw0zlQUx1EoiIhT/frrr9cNhQULFlChQoUyrOjO5vTBa+IYqamphIeHk5WVRWhoKD4+PixYsABXV1fq16/Pyy+/jNlsZtq0aeTk5JCdnU1ISAhhYWF88803xMbGUqVKFVxcXDRgUBzqX//6Fz/99BMJCQl8//33mM1mzp49y6hRo3jkkUfo2rUrmzdvdnaZdwyFQjnl5ubGO++8w4kTJ3juueewWq2sXLmSu+++m4ULF7Jhwwb8/PwIDAzk0Ucf5fTp0wwZMoSwsDBmz57N/Pnz8fHxITo62tmrIuXciBEj+PHHH2nbti0PPvgg7du35//+7/+Ij4/nkUcecXZ5dxyFQjnVokULTCYTNWvW5OTJk7i4uBAREQHApUuX6NixI126dOH9999ny5YteHl5UVBQAMDp06fx8fEBLo84P3bsmNPWQ+4cNWvWZPHixSQmJmIymYztUcqWQqGcMplMxu/VqlWjUqVKLFq0CG9vb7Zt28Zdd93Fu+++S+vWrQkLC+PLL79kx44dwOV/zkOHDtG4cWO+++47qlSp4qzVkDuAi4sLVquV1157jZCQELp06cL69evZsGGDs0u7IykU7gAuLi5MmzaNYcOGYbPZ8PT0ZO7cuZhMJmbMmMGHH35I1apVcXV1JT8/n7i4OCZPnoynpyeenp4KBXGou+++G4vFwsGDB4mJieHNN9+kbt26ZGdnO7u0O5IucyEiIgadkioiIgaFgoiIGBQKIiJiUCiIiIhBoSAiIgadkipyA8ePH6dv3774+fkZ09q3b8/o0aOdWJWIYygUROzwl7/8hWXLljm7DBGHUyiI/AmFhYVERUVx6tQpsrOz6dy5MxERERw9epQXX3wRi8VCxYoVWbBgAWazmenTp2M2m/Hw8GDmzJnUrVvX2asgclUavCZyA1frPoqIiODw4cOEhIRgNpvp3LkzX331FSNHjiQ0NJTOnTuzadMmKleuTGJiIv3796dLly6kpKSQmJjI/PnznbhGItemPQURO/yx+yg3N5eNGzfy5Zdf4uXlRX5+PgBHjhyhTZs2APTu3RuA2NhY3nzzTd5++21sNhvu7u5lvwIidlIoiPwJSUlJeHt78/LLL/Pzzz+zdu1abDabcRHBhx56iOTkZM6dO0ejRo0IDw+nbdu2HDp0iK+//trZ5Ytck0JB5E/o0KEDEyZMYM+ePVSqVIkGDRqQmZnJpEmTiIqKYvHixVSsWJG4uDgefvhhZsyYgdls5tKlS0ybNs3Z5Ytck44piIiIQYPXRETEoFAQERGDQkFERAwKBRERMSgURETEoFAQERGDQkFERAz/H0u7+By03seGAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "run coin_flipping_staticviz.py 500 # flip it 500 times"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "ename": "SyntaxError",
     "evalue": "invalid syntax (<ipython-input-7-b267fd5f8e37>, line 2)",
     "output_type": "error",
     "traceback": [
      "\u001b[1;36m  File \u001b[1;32m\"<ipython-input-7-b267fd5f8e37>\"\u001b[1;36m, line \u001b[1;32m2\u001b[0m\n\u001b[1;33m    run coin_flipping_dynamicviz.py 50 1\u001b[0m\n\u001b[1;37m                               ^\u001b[0m\n\u001b[1;31mSyntaxError\u001b[0m\u001b[1;31m:\u001b[0m invalid syntax\n"
     ]
    }
   ],
   "source": [
    "%matplotlib qt # we can't use this yet, don't have the permission to run animations\n",
    "run coin_flipping_dynamicviz.py 50 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
