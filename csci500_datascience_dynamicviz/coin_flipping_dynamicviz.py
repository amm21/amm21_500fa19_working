# coin_flipping_dynamicviz.py 

"""
Graphing frequencies of coin flips using Seaborn.
"""

from matplotlib import animation
import matplotlib.pyplot as plt
import numpy as np
import random
import seaborn as sns
import sys

# this is the function that FuncAnimation will use for repeatedly re-drawing
# the animation
def update( frame_number, number_of_flips, faces, flips ):
    # note: number_of_flips is PER ANIMATION FRAME
    # flip coin by number_of_flips times for this animation frame
    for i in range(number_of_flips):
        flips[random.randrange(2)] += 1     
        
    plt.cla() # clear axes (what's plotted in the chart, not the whole plot) for the next frame
    
    # the rest of the code comes from the staticviz version
    
    title = f'Flipping a coin {sum(flips):,} Times'
    sns.set_style('whitegrid')  # white backround with gray grid lines
    axes = sns.barplot(faces, flips, palette='bright')  # create bars
    axes.set_title(title)  # set graph title
    axes.set(xlabel='Face', ylabel='Frequency')  # label the axes

    # scale y-axis by 10% to make room for text above bars
    axes.set_ylim(top=max(flips) * 1.10)

    # display frequency & percentage above each patch (bar)
    for bar, flip in zip(axes.patches, flips):
        text_x = bar.get_x() + bar.get_width() / 2.0  
        text_y = bar.get_height() 
        text = f'{flip:,}\n{flip / sum(flips):.3%}'
        axes.text(text_x, text_y, text, 
                  fontsize=11, ha='center', va='bottom')
        

        
# rather than use a list comprehension to pre-generate the coin flips (as with
# the staticviz version), we will start with a list of two zeros, set up the labels
# along the x-axis, and get the number of animation frames and the number of coin flips
# per frame (from command-line arguments)
# these references will then be passed as arguments to the FuncAnimation method call below

flips = [0, 0] # analogous to frequencies in dierolling
faces = ['heads', 'tails'] # analogous to values in dierolling

# read command-line arguments for number of frames and flips per frame
number_of_frames = int(sys.argv[1]) # this corresponds to the number of frames (ex "6000") 
flips_per_frame = int(sys.argv[2])  # this corresponds to the number of flips per frame (ex "1")
# note: the argument at index 0 is the file name

figure = plt.figure("Flipping a coin") # title will be overwritten during animation (title and set_title)

coin_animation = animation.FuncAnimation( figure, update, repeat=False, frames=number_of_frames, interval=33,
                                        fargs=( flips_per_frame, faces, flips )) 
# even though we don't use the variable coin_animation explicitly, we do still need to assign the FuncAnimation
# object to a reference variable and it is used internally

plt.show() # display graph