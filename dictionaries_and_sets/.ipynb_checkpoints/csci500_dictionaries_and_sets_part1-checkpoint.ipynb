{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# **CSCI 500: Dictionaries and Sets (part 1)**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Objectives (across all parts of this unit)\n",
    "In this unit, you’ll:\n",
    "* Use dictionaries to represent unordered collections of key–value pairs.\n",
    "* Create, initialize and refer to elements of dictionaries. \n",
    "* Iterate through a dictionary’s keys, values and key–value pairs.\n",
    "* Combine sets with set operators and methods. \n",
    "* Use operators `in` and `not` `in` to determine if a dictionary contains a key or a set contains a value.\n",
    "* Use the mutable set operations to modify a set’s contents.\n",
    "* Use comprehensions to create dictionaries and sets quickly and conveniently.\n",
    "* Learn how to build dynamic visualizations and implement more of your own in the exercises.\n",
    "* Enhance your understanding of mutability and immutability."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "* A **dictionary** is an _unordered_ collection which stores **key–value pairs** that map immutable keys to values, just as a conventional dictionary maps words to definitions. \n",
    "    * Other programming languages, like Java, might use the term* `map` to refer to this kind of collection or data structure.\n",
    "    * More generically, we might describe a dictionary or map as a \"lookup table\" \n",
    "    > Consider a table of students, where the *keys* are the students' unique IDs (or, back in the day, we used social security numbers), and the *values* could be the students' names (which might not necessarily be unique)\n",
    "* A **set** is an *unordered* collection of unique immutable elements.\n",
    "    * Any collection where the order doesn't matter and each element is unique could be considered a set, such as the names of all colors that can be accessed by name in a CSS style sheet (https://www.w3schools.com/cssref/css_colors.asp)\n",
    "    * But also: the keys of a given dictionary are all unique and are not necessarily ordered, so we *might* consider this as the dictionary's \"set of keys,\" right? *More on this later...*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dictionaries\n",
    "* A dictionary _associates_ keys with values. \n",
    "* Each key _maps_ to a specific value. \n",
    "* Sample dictionary keys and values:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Keys | Key type | Values | Value type\n",
    "| :-------- | :-------- | :-------- | :--------\n",
    "| Country names | `str` | Internet country codes | `str` \n",
    "| Decimal numbers | `int` | Roman numerals | `str` \n",
    "| States | `str` | Agricultural products | list of `str` \n",
    "| Hospital patients | `str`  | Vital signs | tuple of `int`s and `float`s \n",
    "| Baseball players | `str`  | Batting averages | `float` \n",
    "| Metric measurements | `str`  | Abbreviations | `str` \n",
    "| Inventory codes | `str`  | Quantity in stock | `int` "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Unique Keys\n",
    "* Keys must be _immutable_ (strings, numbers, or tuples) and _unique_.\n",
    "* Multiple keys can have the same value. (That is, in a given dictionary, all **keys** must be **unique**, but the dictionary **can** have **duplicate values**.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating a Dictionary\n",
    "* Create a dictionary by enclosing in curly braces, `{}`, <u>a comma-separated list of **key–value pairs**</u>, each of the form _key_: _value_. \n",
    "* Create an empty dictionary with `{}`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First key-value pair is 'Finland': 'fi'\n",
    "# Second key-value pair is 'South Africa': 'za'\n",
    "# Third key-value pair is 'Nepal': 'np'\n",
    "country_codes = {'Finland': 'fi', \n",
    "                 'South Africa': 'za', \n",
    "                 'Nepal': 'np'}\n",
    "                 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'Finland': 'fi', 'South Africa': 'za', 'Nepal': 'np'}"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "country_codes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Dictionaries are _unordered_ collections, **unlike lists and tuples**.\n",
    "* **Do _not_ write code that depends on the _order_ of the key–value pairs.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Determining if a Dictionary Is Empty "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(country_codes) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this is an easy way to create an empty dictionary..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "an_empty_dict = {}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(an_empty_dict)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ...but you can also use the built-in function `dict` with no arguments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "another_empty_dict = dict()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(another_empty_dict)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Can use a dictionary as a condition to determine if it’s empty—non-empty is `True` and empty is `False`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "country_codes is not empty\n"
     ]
    }
   ],
   "source": [
    "# note: this is for empty/not empty, NOT exists/doesn't exist\n",
    "if country_codes:\n",
    "    print('country_codes is not empty')\n",
    "else:\n",
    "    print('country_codes is empty')\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "country_codes.clear() # the dictionary method `clear` will delete the dictionary's key-value pairs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "country_codes is empty\n"
     ]
    }
   ],
   "source": [
    "if country_codes:\n",
    "    print('country_codes is not empty')\n",
    "else:\n",
    "    print('country_codes is empty')\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *Self-test questions: Creating a Dictionary*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(Fill in the blank)** `_________` can be thought of as unordered collections in which each value is accessed through its corresponding key. "
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Answer: Dictionaries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(True or false; if false, explain or correct)** Dictionaries may contain duplicate keys."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Answer: False. Dictionary keys must be unique but the values can be duplicates."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(Code in an IPython session, then copy your statements below)** Create a dictionary named `states` that maps three (3) state abbreviations to their state names. Then, display the dictionary (that is, just execute `states` on its own line)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "states = { 'SC': 'South Carolina', \n",
    "           'NE': 'Nebraska', \n",
    "           'PA': 'Pennsylvania'}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'SC': 'South Carolina', 'NE': 'Nebraska', 'PA': 'Pennsylvania'}"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "states"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Iterating through a Dictionary "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Here's an example of a dictionary where \n",
    "# MULTIPLE KEYS have the SAME VALUE (which is OK, as long as\n",
    "# all the KEYS themselves are UNIQUE!)\n",
    "days_per_month = {'January': 31, 'February': 28, 'March': 31}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'January': 31, 'February': 28, 'March': 31}"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "days_per_month"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Dictionary method **`items`** returns each key–value pair as a tuple: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "January has 31 days\n",
      "February has 28 days\n",
      "March has 31 days\n"
     ]
    }
   ],
   "source": [
    "for month, days in days_per_month.items():\n",
    "    print(f'{month} has {days} days')\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *Self-test question: Iterating through a Dictionary*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(Fill in the blank)** The dictionary method `________` returns each key-value pair as a tuple."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Answer: items"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic Dictionary Operations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* In the example below, we have intentionally provided the incorrect value `100` for the key `'X'` (we will correct this in a later snippet):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [],
   "source": [
    "roman_numerals = {'I': 1, 'II': 2, 'III': 3, 'V': 5, 'X': 100}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'I': 1, 'II': 2, 'III': 3, 'V': 5, 'X': 100}"
      ]
     },
     "execution_count": 55,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "roman_numerals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing the Value Associated with a Key"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5"
      ]
     },
     "execution_count": 56,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# since dictionaries are UNORDERED, the keys themselves serve as the indexes\n",
    "# for looking up and retrieving their corresponding values:\n",
    "roman_numerals['V']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Updating the Value of an Existing Key–Value Pair"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Here, we update the incorrect value for \n",
    "# the Roman numeral 'X' from 100 to 10\n",
    "roman_numerals['X'] = 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'I': 1, 'II': 2, 'III': 3, 'V': 5, 'X': 10}"
      ]
     },
     "execution_count": 58,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "roman_numerals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding a New Key–Value Pair"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {},
   "outputs": [],
   "source": [
    "roman_numerals['L'] = 50 # this adds the key-value pair 'L': 50 to the dictionary..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'I': 1, 'II': 2, 'III': 3, 'V': 5, 'X': 10, 'L': 50}"
      ]
     },
     "execution_count": 60,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "roman_numerals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* String keys are case sensitive. \n",
    "* Assigning to a nonexistent key inserts a new key–value pair. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [
    {
     "ename": "KeyError",
     "evalue": "'x'",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mKeyError\u001b[0m                                  Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-61-5cdb18f240d2>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[0mroman_numerals\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;34m'x'\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;31mKeyError\u001b[0m: 'x'"
     ]
    }
   ],
   "source": [
    "roman_numerals['x']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Removing a Key–Value Pair"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [],
   "source": [
    "del roman_numerals['III']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'I': 1, 'II': 2, 'V': 5, 'X': 10, 'L': 50}"
      ]
     },
     "execution_count": 63,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "roman_numerals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Method **`pop`** returns the value for the removed key."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 64,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "roman_numerals.pop('X') # removes the key-value pair AND returns the key's value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'I': 1, 'II': 2, 'V': 5, 'L': 50}"
      ]
     },
     "execution_count": 65,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "roman_numerals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Attempting to Access a Nonexistent Key"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "metadata": {},
   "outputs": [
    {
     "ename": "KeyError",
     "evalue": "'III'",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mKeyError\u001b[0m                                  Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-66-ccd50c7f0c8b>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[0mroman_numerals\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;34m'III'\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;31mKeyError\u001b[0m: 'III'"
     ]
    }
   ],
   "source": [
    "roman_numerals['III']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Method **`get`** returns its argument’s corresponding value or `None` if the key is not found. \n",
    "* IPython does not display anything for `None`. \n",
    "* Using `get` with a **second argument** returns the second argument *if* the key is not found."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 67,
   "metadata": {},
   "outputs": [],
   "source": [
    "roman_numerals.get('III') \n",
    "# this is a safer/better way than trying to access it using the key as an index (subscript), like roman_numerals['III']\n",
    "# because if it is not found, the subscript version will error"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 68,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'III not in dictionary'"
      ]
     },
     "execution_count": 68,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "roman_numerals.get('III', 'III not in dictionary')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 69,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "50"
      ]
     },
     "execution_count": 69,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# What happens if you use `get` with a second argument when the key IS found?\n",
    "# if the key IS found, you get the value and the second argument doesn't print\n",
    "roman_numerals.get('L', 'L not in dictionary')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 70,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "L is in the dictionary\n"
     ]
    }
   ],
   "source": [
    "# the \"if/else\" from above for empty or not empty that was used with lists and variables also works here\n",
    "if roman_numerals.get('L'):\n",
    "    print('L is in the dictionary')\n",
    "else:\n",
    "    print('L is not in the dictionary')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Testing Whether a Dictionary Contains a Specified Key"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 74,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "'V' in roman_numerals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 75,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "'III' in roman_numerals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 76,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "'III' not in roman_numerals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *Self-test questions: Basic Dictionary Operations*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(True or false; if false, explain or correct)** Assigning to a nonexistent dictionary key causes an exception to be thrown."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Answer: False. Assigning a value to a nonexistent dictionary key will insert a new key-value pair into the dictionary.\n",
    "Sometimes this is exactly what you want to do, but if not (such as if you mistype the key), then this would be a logic error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(Short answer)** What does a statement of the following form do when the *key* is in the dictionary? \n",
    "\n",
    "*dictionaryName[key] = value*"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Answer: This updates the value associated with the key, replacing the original/old value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(Code in an IPython session, then copy your statements below)** String dictionary keys are *case-sensitive*. Confirm this by using the following dictionary and assigning `10` to the (lowercase) key `'x'`&mdash;doing so adds a *new* key-value pair rather than correcting the value for the (uppercase) key `'X'`:  \n",
    "\n",
    "```python\n",
    "roman_numerals = { 'I': 1, 'II': 2, 'III': 3, 'V': 5, 'X': 100 }\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 77,
   "metadata": {},
   "outputs": [],
   "source": [
    "# I renamed this because I didn't want to mess with my notes from earlier\n",
    "roman_numerals_selftest = { 'I': 1, 'II': 2, 'III': 3, 'V': 5, 'X': 100 }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 78,
   "metadata": {},
   "outputs": [],
   "source": [
    "roman_numerals_selftest['x'] = 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 79,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'I': 1, 'II': 2, 'III': 3, 'V': 5, 'X': 100, 'x': 10}"
      ]
     },
     "execution_count": 79,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "roman_numerals_selftest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dictionary Methods `keys` and `values` "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "months = {'January': 1, 'February': 2, 'March': 3}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 81,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "January  February  March  "
     ]
    }
   ],
   "source": [
    "for month_name in months.keys():\n",
    "    print(month_name, end='  ')\n",
    "   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 82,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1  2  3  "
     ]
    }
   ],
   "source": [
    "for month_number in months.values():\n",
    "    print(month_number, end='  ')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dictionary **Views**\n",
    "* Methods `items`, `keys` and `values` each return a **view** of a dictionary’s data. \n",
    "* When you iterate over a **`view`**, it “sees” the dictionary’s **current contents**—it does **not** have its own copy of the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "months_view = months.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dict_keys(['January', 'February', 'March'])"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "months_view"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "January  February  March  "
     ]
    }
   ],
   "source": [
    "for key in months_view:\n",
    "    print(key, end='  ')\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "months['December'] = 12"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'January': 1, 'February': 2, 'March': 3, 'December': 12}"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "months"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 88,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "January  February  March  December  "
     ]
    }
   ],
   "source": [
    "for key in months_view:\n",
    "    print(key, end='  ')\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**NOTE:** You should never attempt to modify a dictionary while iterating through a view &mdash; either you will get a `RuntimeError`, or the loop might not process all (or any!) of the view's values. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Before:  January\n",
      "After:  Smarch\n",
      "Before:  February\n",
      "After:  Smarch\n",
      "Before:  March\n",
      "After:  Smarch\n",
      "Before:  December\n",
      "After:  Smarch\n"
     ]
    }
   ],
   "source": [
    "# keep in mind, you can't modify a key period so this would still not work even\n",
    "# if you were using the dict itself instead of the view version\n",
    "# this can only be done if you delete the key and re-add with the new one you want\n",
    "for key in months_view:\n",
    "    print(\"Before: \", key)\n",
    "    key = 'Smarch'\n",
    "    print(\"After: \", key)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dict_keys(['January', 'February', 'March', 'December'])"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "months_view"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "# So how CAN we iterate through a dictionary and modify its values?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "# you CAN modify the values, just not keys\n",
    "for key in months:\n",
    "    months[key] = 999"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'January': 999, 'February': 999, 'March': 999, 'December': 999}"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "months"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Converting Dictionary Keys, Values and Key–Value Pairs to Lists"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Occasionally you may need *lists* of a dictionary's keys, values, or key-value pairs. To obtain such a list, pass the view returned by `keys`, `values`, or `items` to the built-in `list` function. (Also, modifying these lists will *not* modify the underlying dictionary.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['January', 'February', 'March', 'December']"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(months.keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[999, 999, 999, 999]"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(months.values())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[('January', 999), ('February', 999), ('March', 999), ('December', 999)]"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# this one makes a list of tuples, each tuple giving the key-value pair\n",
    "list(months.items())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [],
   "source": [
    "months_from_tuples = dict(list(months.items()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'January': 999, 'February': 999, 'March': 999, 'December': 999}"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "months_from_tuples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Processing Keys in Sorted Order using the built-in `sorted` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [],
   "source": [
    "months = {'January': 1, 'February': 2, 'March': 3, 'December': 12}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "December  February  January  March  "
     ]
    }
   ],
   "source": [
    "for month_name in sorted(months.keys()):\n",
    "    print(month_name, end='  ')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "December is the 12th month of the year\n",
      "February is the 2nd month of the year\n",
      "January is the 1st month of the year\n",
      "March is the 3rd month of the year\n"
     ]
    }
   ],
   "source": [
    "for month_name in sorted(months.keys()):\n",
    "    \"\"\"\n",
    "    suffix = 'th'\n",
    "    # alternatively, suffix can be empty and you can use an else that\n",
    "    # assigns it to 'th'\n",
    "    \n",
    "    if months[month_name] == 1:\n",
    "        suffix = 'st'\n",
    "    elif months[month_name] == 2:\n",
    "        suffix = 'nd'\n",
    "    elif months[month_name] == 3:\n",
    "        suffix = 'rd'\n",
    "    \"\"\"\n",
    "    \n",
    "    # this solves it using a conditional expression/ternary operation\n",
    "    suffixes = ['st', 'nd', 'rd']\n",
    "    month_index = months[month_name]-1\n",
    "    suffix = suffixes[month_index] if month_index in range(3) else 'th'\n",
    "    print(f'{month_name} is the {months[month_name]}{suffix} month of the year')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## *Self-test questions: Dictionary methods* `keys` *and* `values`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(Fill in the blank)** Dictionary method `__________` returns an unordered list of the dictionary's keys:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Answer: keys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(True or false; if false, explain or correct)** A view has its own copy of the corresponding data from the dictionary."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Answer: False. It \"sees\" the current data in that dict, it does not have or make a copy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(IPython Session)** Create a dictionary that consists of the Roman numerals I, II, III, IV, and V and their corresponding values using Arabic numbers. Then, create lists of its keys, values, and items (and show those lists):   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
