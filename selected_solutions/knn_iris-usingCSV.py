"""
Repeat the KNN analysis for the Iris dataset

When running from an interactive IPython console,
don't forget to use the %matplotlib magic so that 
figures will be drawn (by default, in a new window)
"""



# TODO #1:
# Let's say you're reading in the Iris dataset from a CSV file
# instead of using the version built into SciKit-learn. First,
# we'll need to replace the code for reading in the data with 
# code that allows you to read in a CSV file as a DataFrame object...
"""
# Read in the dataset and store in a 'Bunch' object
from sklearn.datasets import load_iris
iris = load_iris() 
"""
import pandas as pd
iris = pd.read_csv('\..\..\abalone.csv')

# TODO #2: Display the first few rows so you know it works
iris.head()

# TODO #3:
# Since we're not using Bunch objects, which have `data` and `target`
# attributes, we need to parse the DataFrame so that the data (feature) columns
# are stored in one DataFrame and the target (species) column is stored
# as a pandas Series. THE CODE BELOW ASSUMES THAT THE LAST (RIGHTMOST) COLUMN 
# IS THE ONE THAT CONTAINS THE TARGET VALUES:

index_of_last_column = len(iris.columns) - 1

iris_data = iris.iloc[:, 0:index_of_last_column] # feature (data) columns
iris_target = iris.iloc[:, index_of_last_column] # species (target) column

# TODO #4:
# Since the `iris` DataFrame does not have `data` and `target` attributes
# like a Bunch object does, then we will replace every instance of 
# `iris.data` with `iris_data`, and we will replace every instance of
# 'iris.target` with `iris_target`. I did this with two separate 
# search-and-replace operations (in Spyder, choose Search > Replace Text,
# and only replaced those instances BELOW this comment.)

# **** SCROLL DOWN TO LINE 131 to see TODO #5 ****

# Checking the Sample and Target Sizes
print( "# of samples and features for each sample: " )
print( iris_data.shape )
print( "# of targets (labels); should match # of samples: " )
print( iris_target.shape )


# Splitting the Data for Training and Testing
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(
    iris_data, iris_target, random_state=11)  # random_state for reproducibility

# NOTE: X is a matrix containing vectors (rows) of features
#       for each data sample. y is a vector that contains 
#       the expected classes (the "ground truth") for each data sample.


# Training and Testing Set Sizes
print( "# of samples (and features for each sample) in training partition: " ) 
print( X_train.shape )
print( "# of samples (and features for each sample) in testing partition:  " ) 
print( X_test.shape )
print() # blank line for readability

# Creating the Model
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier()

# Training the Model
# (Note: unlike most other supervised classification methods,
#        there's really no "model" in kNN -- we're basically 
#        just setting aside the training data, and the actual
#        computations will be performed in the prediction step)
knn.fit(X=X_train, y=y_train)

# Predicting Iris Classes
predicted = knn.predict(X=X_test)
expected = y_test


print( "Predicted" )
print( "---------" )
print( predicted )
print() # blank line for readability

print( "Expected" )
print( "--------" )
print( expected )
print() # blank line for readability

wrong = [(p, e) for (p, e) in zip(predicted, expected) if p != e]

print( "Number of incorrect identifications")
print( "-----------------------------------")
print( wrong )
print() # blank line for readability

print("Classification accuracy (from explicit computation)")
print("---------------------------------------------------")
print(f'{(len(expected) - len(wrong)) / len(expected):.2%}')
print() # blank line for readability

# Estimator Method score
print("Classification accuracy (using estimator method `score`)")
print("--------------------------------------------------------")
print(f'{knn.score(X_test, y_test):.2%}')
print() # blank line for readability

# Confusion Matrix
from sklearn.metrics import confusion_matrix
confusion = confusion_matrix(y_true=expected, y_pred=predicted)


print("Confusion matrix")
print("----------------")
print( confusion )
print() # blank line for readability

# Classification Report
from sklearn.metrics import classification_report

# TODO #5: Replace all instances of 
# `iris_target_names` (or `iris.target_names`) with `iris_target.unique()`
# (And that's it!!)

print("Classification report")
print("---------------------")
print( classification_report( expected, predicted, 
                              target_names=iris_target.unique() ) ) 
print() # blank line for readability

print("Classification report definitions:")
print("---------------------------------")
print("Precision, for a given class, is the fraction of ALL samples predicted")
print("  as belonging to that class that were CORRECTLY predicted as belonging to that class.")
print("  For example: of ALL samples that our classifier predicted as `Setosa`, ")
print("  how many of those did we get right?")      
print("  Precision is also known as the 'Positive Predictive Value', or PPV")
print("Recall, for a given class, tells you the fraction of samples ACTUALLY belonging")
print("  to that class that were CORRECTLY predicted as belonging to that class.")
print("  For example: of all `Setosa` samples in our dataset, ")
print("  how many did we correctly identify as such? That is, even if we got a bunch")
print("  of other predictions, did we at least identify all of the `Setosa` samples?")        
print("  Recall is also known as 'Sensitivity'.")
print() # blank line for readability

# Visualizing the Confusion Matrix
import pandas as pd
confusion_df = pd.DataFrame(confusion, index=iris_target.unique(), columns=iris_target.unique())

# don't forget to execute the %matplotlib magic 
# when your kernel starts; otherwise the plots won't
# be generated when running the script from an 
# interactive IPython console      
import matplotlib.pyplot as plt
import seaborn as sns

plt.tight_layout()

axes = sns.heatmap( confusion_df, annot=True, cmap='nipy_spectral_r')
axes.set_ylim(len(confusion_df), 0)
plt.yticks( va="center" ) # vertically center the Y-axis tickmarks

# I also added a title (which you should do too!)
plt.title("k-NN confusion matrix for Iris dataset\n"
          + "(Values along diagonal are CORRECT classifications)")

# show plot in window
plt.show()

# save figure in PNG (raster graphics) and EPS (vector graphics) image formats
plt.savefig('knn_iris_heatmap.png', format='png')
plt.savefig('knn_iris_heatmap.eps', format='eps')

# Perform K-Fold Cross-Validation (here, using 4 splits)
from sklearn.model_selection import KFold
kfold = KFold(n_splits=4, random_state=11, shuffle=True)

# Using the KFold Object with Function cross_val_score
from sklearn.model_selection import cross_val_score
scores = cross_val_score(estimator=knn, X=iris_data, y=iris_target, cv=kfold)

print("Cross-validation results:")
print("-------------------------")
print( scores )
print() # blank line for readability

print(f'Mean accuracy: {scores.mean():.2%}')
print() # blank line for readability

print(f'Accuracy standard deviation: {scores.std():.2%}')
print() # blank line for readability

# Perform Hyperparameter Tuning (which value of 'k' is best?)
# NOTE: the "k" in "kNN" has a different meaning than
#       the "k" in "k-fold cross-validation"!!"
print("Hyperparameter tuning results:")
print("------------------------------")
for k in range(1, 20, 2):  # k is an odd value 1-19; odds prevent ties
    kfold = KFold(n_splits=4, random_state=11, shuffle=True)
    knn = KNeighborsClassifier(n_neighbors=k)
    scores = cross_val_score(estimator=knn, 
        X=iris_data, y=iris_target, cv=kfold)
    print(f'k={k:<2}; mean accuracy={scores.mean():.2%}; ' +
          f'standard deviation={scores.std():.2%}')
    
print() # blank line for readability 

