{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# CSCI B500 &mdash; Files and Exceptions (please go through this on your own)\n",
    "\n",
    "# Objectives\n",
    "* Understand the notions of files and persistent data. \n",
    "* Read, write and update files.\n",
    "* Read and write CSV files, a common format for machine-learning datasets.\n",
    "* Serialize objects into the JSON data-interchange format—commonly used to transmit over the Internet—and deserialize JSON into objects.\n",
    "* Use the `with` statement to ensure that resources are properly released, avoiding “resource leaks.”\n",
    "* Use the `try` statement to delimit code in which exceptions may occur and handle those exceptions with associated `except` clauses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Use the `try` statement’s `else` clause to execute code when no exceptions occur in the `try` suite.\n",
    "* Use the `try` statement’s `finally` clause to execute code regardless of whether an exception occurs in the `try`. \n",
    "* `raise` exceptions to indicate runtime problems.\n",
    "* Understand the traceback of functions and methods that led to an exception."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------              "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "* Variables, lists, tuples, dictionaries, sets, arrays, pandas `Series` and pandas `DataFrame`s offer only _temporary_ data storage\n",
    "    * lost when a local variable “goes out of scope” or when the program terminates\n",
    "* Data maintained in **files** is persistent\n",
    "* Computers store files on secondary storage devices\n",
    "    * solid-state drives, hard disks and more"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* We consider text files in several popular formats\n",
    "    * plain text\n",
    "    * JSON (JavaScript Object Notation)\n",
    "    * CSV (comma-separated values)\n",
    "* Use JSON to serialize and deserialize objects for saving to secondary storage and transmitting them over the Internet\n",
    "* Intro to Data Science section shows loading and manipulating CSV data with\n",
    "    * Python Standard Library’s `csv` module \n",
    "    * pandas Library"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Python security&mdash;we’ll discuss the security vulnerabilities of serializing and deserializing data with the Python Standard Library’s `pickle` module\n",
    "    * <span style=\"background: yellow;\">It is recommended to use JSON serialization in preference to `pickle`</span>\n",
    "* **exception handling**\n",
    "    * An exception indicates an execution-time problem\n",
    "    * E.g., `ZeroDivisionError`, `NameError`, `ValueError`, `StatisticsError`, `TypeError`, `IndexError`, `KeyError` and `RuntimeError`\n",
    "    * Deal with exceptions as they occur by using `try` statements and `except` clauses \n",
    "    * Help you write _robust_, _fault-tolerant_ programs that can deal with problems and continue executing or _terminate gracefully_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Programs typically request and release resources (such as files) during program execution\n",
    "* Often, these are in limited supply or can be used only by one program at a time\n",
    "* You'll see how to guarantee that after a program uses a resource, it’s released for use by other programs, even if an exception has occurred\n",
    "* Use the `with` statement for this purpose"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------           "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Files \n",
    "* A **text file** is a sequence of characters \n",
    "* A **binary file** (for images, videos and more) is a sequence of bytes\n",
    "* First character in a text file or byte in a binary file is located at position 0\n",
    "    * In a file of **_n_** characters or bytes, the highest position number is **_n_ – 1**\n",
    "* For each file you **open**, Python creates a **file object** that you’ll use to interact with the file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Conceptual view of a file](file_io_images/AAEMYSR0.png \"Conceptual view of a file\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### End of File\n",
    "* Every operating system provides a mechanism to denote the end of a file\n",
    "    * Some use an **end-of-file marker**\n",
    "    * Others maintain a count of the total characters or bytes in the file\n",
    "    * Programming languages hide these operating-system details from you "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Standard File Objects\n",
    "* When a Python program begins execution, it creates three **standard file objects**:\n",
    "    * **`sys.stdin`**—the **standard input file object**\n",
    "    * **`sys.stdout`**—the **standard output file object**, and \n",
    "    * **`sys.stderr`**—the **standard error file object**. \n",
    "* Though considered file objects, they do not read from or write to files by default\n",
    "    * The `input` function implicitly uses `sys.stdin` to get user input from the keyboard\n",
    "    * Function `print` implicitly outputs to `sys.stdout`, which appears in the command line\n",
    "    * Python implicitly outputs program errors and tracebacks to `sys.stderr`, which also appears in the command line\n",
    "* Import the `sys` module if you need to refer to these objects explicitly in your code&mdash;this is rare "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------             "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Text-File Processing\n",
    "* Here, we'll create a simple text file that might be used by an accounts-receivable system to track the money owed by a company’s clients\n",
    "* We’ll then read that text file to confirm that it contains the data\n",
    "* For each client, we’ll store \n",
    "    * client’s account number\n",
    "    * last name \n",
    "    * account balance owed to the company\n",
    "* These data fields represent a client **record**\n",
    "* Programmers must structure files to meet their applications’ requirements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Writing to a Text File: Introducing the `with` Statement \n",
    "* Many applications _acquire_ resources\n",
    "    * files, network connections, database connections and more\n",
    "* Should _release_ resources as soon as they’re no longer needed\n",
    "* Ensures that other applications can use the resources\n",
    "* `with` statement \n",
    "    * Acquires a resource and assigns its corresponding object to a variable\n",
    "    * Allows the application to use the resource via that variable\n",
    "    * Calls the resource object’s **`close` method** to release the resource"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('accounts.txt', mode='w') as accounts:\n",
    "    accounts.write('100 Jones 24.98\\n')\n",
    "    accounts.write('200 Doe 345.67\\n')\n",
    "    accounts.write('300 White 0.00\\n')\n",
    "    accounts.write('400 Stone -42.16\\n')\n",
    "    accounts.write('500 Rich 224.62\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Can also **write to a file with `print`**, which **automatically outputs a `\\n`**, as in\n",
    ">```python\n",
    "print('100 Jones 24.98', file=accounts)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# macOS/Linux Users: View file contents\n",
    "!cat accounts.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Windows Users: View file contents\n",
    "!more accounts.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Built-In Function `open` \n",
    "* Opens the file `accounts.txt` and associates it with a file object\n",
    "* `mode` argument specifies the **file-open mode**\n",
    "    * whether to open a file for reading from the file, for writing to the file or both. \n",
    "* Mode `'w'` opens the file for _writing_, creating the file if it does not exist\n",
    "* If you do not specify a path to the file, Python creates it in the current folder \n",
    "* **Be careful**—opening a file for writing _deletes_ all the existing data in the file\n",
    "* By convention, the **`.txt` file extension** indicates a plain text file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Writing to the File \n",
    "* `with` statement assigns the object returned by `open` to the variable `accounts` in the **`as` clause**\n",
    "* `with` statement’s suite uses `accounts` to interact with the file\n",
    "    * file object’s **`write` method** writes one record at a time to the file\n",
    "* At the end of the `with` statement’s suite, the `with` statement _implicitly_ calls the file object’s **`close`** method to close the file "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------                "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading Data from a Text File\n",
    "* Let's read `accounts.txt` sequentially from beginning to end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('accounts.txt', mode='r') as accounts:\n",
    "    print(f'{\"Account\":<10}{\"Name\":<10}{\"Balance\":>10}')\n",
    "    for record in accounts:\n",
    "        account, name, balance = record.split()\n",
    "        print(f'{account:<10}{name:<10}{balance:>10}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* If the contents of a file should not be modified, open the file for reading only\n",
    "    * Prevents program from accidentally modifying the file\n",
    "* Iterating through a file object, reads one line at a time from the file and returns it as a string\n",
    "* For each `record` (that is, line) in the file, string method `split` returns tokens in the line as a list\n",
    "    * We unpack into the variables `account`, `name` and `balance`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### File Method `readlines`\n",
    "* File object’s **`readlines`** method also can be used to read an _entire_ text file\n",
    "* Returns each line as a string in a list of strings\n",
    "* For small files, this works well, but iterating over the lines in a file object, as shown above, can be more efficient\n",
    "    * Enables your program to process each text line as it’s read, rather than waiting to load the entire file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Seeking to a Specific File Position\n",
    "* While reading through a file, the system maintains a **file-position pointer** representing the location of the next character to read\n",
    "* To process a file sequentially from the beginning _several times_ during a program’s execution, you must reposition the file-position pointer to the beginning of the file\n",
    "    * Can do this by closing and reopening the file, or \n",
    "    * by calling the file object’s **`seek`** method, as in \n",
    ">```python\n",
    "file_object.seek(0)\n",
    "```\n",
    "* Obviously, the latter approach is faster"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------              "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Updating Text Files\n",
    "* Formatted data written to a text file cannot be modified without the risk of destroying other data\n",
    "* If the name `'White'` needs to be changed to `'Williams'` in `accounts.txt`, the old name cannot simply be overwritten\n",
    "* The original record for `White` is stored as\n",
    ">```python\n",
    "300 White 0.00\n",
    "```\n",
    "* If you overwrite the name `'White'` with the name `'Williams'`, the record becomes\n",
    ">```python\n",
    "300 Williams00\n",
    "```\n",
    "* The characters beyond the second “`i`” in `'Williams'` overwrite other characters in the line\n",
    "* The problem is that records and their fields can vary in size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* To make the preceding name change, we can: \n",
    "    * copy the records before `300 White 0.00` into a temporary file, \n",
    "    * write the updated and correctly formatted record for account 300 to this file, \n",
    "    * copy the records after `300 White 0.00` to the temporary file, \n",
    "    * delete the old file and \n",
    "    * rename the temporary file to use the original file’s name.\n",
    "* Requires processing _every_ record in the file, even if you need to update only one record\n",
    "    * More efficient when an application needs to update many records in one pass of the file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Updating accounts`.txt` \n",
    "* Update the `accounts.txt` file to change account 300’s name from `'White'` to `'Williams'` as described above: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "accounts = open('accounts.txt', 'r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "temp_file = open('temp_file.txt', 'w')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with accounts, temp_file:\n",
    "    for record in accounts:\n",
    "        account, name, balance = record.split()\n",
    "        if account != '300':\n",
    "            temp_file.write(record)\n",
    "        else:\n",
    "            new_record = ' '.join([account, 'Williams', balance])\n",
    "            temp_file.write(new_record + '\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* This `with` statement manages two resource objects, specified in a comma-separated list after `with`\n",
    "    * If the account is not `'300'`, we write `record` (which contains a newline) to `temp_file`\n",
    "    * Otherwise, we assemble the new record containing `'Williams'` in place of  `'White'` and write it to the file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# macOS/Linux Users ONLY: View file contents\n",
    "!cat temp_file.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Windows Users ONLY: View file contents\n",
    "!more temp_file.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `os` Module File-Processing Functions\n",
    "* To complete the update, delete the old `accounts.txt` file, then rename `temp_file.txt` as `accounts.txt`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.remove('accounts.txt')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Use the **`rename` function** to rename the temporary file as `'accounts.txt'`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.rename('temp_file.txt', 'accounts.txt')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# macOS/Linux Users ONLY: View file contents\n",
    "!cat accounts.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Windows Users ONLY: View file contents\n",
    "!more accounts.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------               "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Serialization with JSON \n",
    "* **JSON (JavaScript Object Notation)** is a text-based, human-and-computer-readable, data-interchange format used to represent objects as collections of name–value pairs. \n",
    "* Preferred data format for transmitting objects across platforms. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### JSON Data Format \n",
    "* Similar to Python dictionaries\n",
    "* Each JSON object contains a comma-separated list of **property names** and **values**, in curly braces. \n",
    "> ```python\n",
    "{\"account\": 100, \"name\": \"Jones\", \"balance\": 24.98}\n",
    "```\n",
    "* JSON arrays, like Python lists, are comma-separated values in square brackets. \n",
    "> ```python\n",
    "[100, 200, 300]\n",
    "```\n",
    "\n",
    "* Values in JSON objects and arrays can be:\n",
    "    * **strings** in **double quotes**\n",
    "    * **numbers**\n",
    "    * JSON Boolean values **`true`** or **`false`** \n",
    "    * **`null`** (like `None` in Python)\n",
    "    * **arrays**  \n",
    "    * **other JSON objects**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python Standard Library Module `json` \n",
    "* **`json` module** enables you to convert objects to JSON (JavaScript Object Notation) text format\n",
    "* Known as **serializing** the data\n",
    "* Following dictionary, which contains one key–value pair consisting of the key `'accounts'` with its associated value being a list of dictionaries representing two accounts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "accounts_dict = {'accounts': [\n",
    "    {'account': 100, 'name': 'Jones', 'balance': 24.98},\n",
    "    {'account': 200, 'name': 'Doe', 'balance': 345.67}]}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Serializing an Object to JSON\n",
    "* Write JSON to a file\n",
    "* `json` module’s **`dump` function** serializes the dictionary `accounts_dict` into the file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('accounts.json', 'w') as accounts:\n",
    "    json.dump(accounts_dict, accounts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Resulting file contains the following text&mdash;reformatted slightly for readability:\n",
    "```python\n",
    "{\"accounts\": \n",
    "  [{\"account\": 100, \"name\": \"Jones\", \"balance\": 24.98}, \n",
    "   {\"account\": 200, \"name\": \"Doe\", \"balance\": 345.67}]}\n",
    "```\n",
    "* JSON delimits strings with _double-quote characters_. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Deserializing the JSON Text\n",
    "* `json` module’s **`load` function** reads entire JSON contents of its file object argument and converts the JSON into a Python object\n",
    "* Known as **deserializing** the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('accounts.json', 'r') as accounts:\n",
    "    accounts_json = json.load(accounts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "accounts_json"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "accounts_json['accounts']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "accounts_json['accounts'][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "accounts_json['accounts'][1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Displaying the JSON Text\n",
    "* `json` module’s **`dumps` function** (`dumps` is short for “dump string”) returns a Python string representation of an object in JSON format\n",
    "* Canbe used to display JSON in a nicely indented format\n",
    "    * sometimes called “pretty printing” \n",
    "* When call includes the `indent` keyword argument, the string contains newline characters and indentation for pretty printing\n",
    "    * Also can use `indent` with the `dump` function when writing to a file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('accounts.json', 'r') as accounts:\n",
    "    print(json.dumps(json.load(accounts), indent=4))\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------              "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"background: yellow;\">Focus on Security:</span> `pickle` Serialization and Deserialization  \n",
    "* Python Standard Library’s **`pickle` module** can serialize objects into in a **Python-specific data format**\n",
    "* **Caution: The Python documentation provides the following warnings about `pickle`**:\n",
    "\n",
    "> “**Pickle files can be hacked**. If you receive a raw pickle file over the network, don’t trust it! It could have malicious code in it, that would run arbitrary Python when you try to de-pickle it. However, if you are doing your own pickle writing and reading, you’re safe (provided no one else has access to the pickle file, of course.)”\n",
    "\n",
    ">“Pickle is a protocol which allows the serialization of arbitrarily complex Python objects. As such, it is specific to Python and **cannot be used to communicate with applications written in other languages**. It is also **insecure by default**: deserializing pickle data coming from an untrusted source can execute arbitrary code, if the data was crafted by a skilled attacker.”"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* We do not recommend using `pickle`, but it’s been used for many years, so you’re likely to encounter it in **legacy code**—old code that’s often no longer supported. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------               "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Additional Notes Regarding Files\n",
    "* Table of file-open modes for text files\n",
    "    * _Reading_ modes raise a `FileNotFoundError` if the file does not exist\n",
    "    * Each text-file mode has a corresponding binary-file mode specified with `b`, as in `'rb'` or `'wb+'`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Mode | Description\n",
    "| ------ | ------\n",
    "| `'r'` | Open a text file for reading. This is the default if you do not specify the file-open mode when you call open. \n",
    "| `'w'` | Open a text file for writing. Existing file contents are _deleted_. \n",
    "| **`'a'`** | Open a text file for appending at the end, creating the file if it does not exist. New data is written at the end of the file. \n",
    "| **`'r+'`** | Open a text file reading and writing. \n",
    "| **`'w+'`** | Open a text file reading and writing. Existing file contents are _deleted_.\n",
    "| **`'a+'`** | Open a text file reading and appending at the end. New data is written at the end of the file. If the file does not exist, it is created. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Other File Object Methods\n",
    "* **`read`** \n",
    "    * For a text file, returns a string containing the number of characters specified by the method’s integer argument\n",
    "    * For a binary file, returns the specified number of bytes\n",
    "    * If no argument is specified, the method returns the entire contents of the file\n",
    "* **`readline`** \n",
    "    * Returns one line of text as a string, including the newline character if there is one\n",
    "    * Returns an empty string when it encounters the end of the file \n",
    "* **`writelines`** \n",
    "    * Receives a list of strings and writes its contents to a file\n",
    "* Classes that Python uses to create file objects are defined in the Python Standard Library’s [**`io` module**](https://docs.python.org/3/library/io.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------              "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Handling Exceptions\n",
    "* Various types of exceptions can occur when you work with files\n",
    "* **`FileNotFoundError`** \n",
    "    * Attempt to open a non-existent file for reading with the `'r'` or `'r+'` modes\n",
    "* **`PermissionsError`** \n",
    "    * Attempt an operation for which you do not have permission\n",
    "    * Try to open a file that your account is not allowed to access \n",
    "    * Create a file in a folder where your account does not have permission to write\n",
    "* `ValueError` \n",
    "    * Attempt to write to a file that has already been closed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Division by Zero and Invalid Input"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Division By Zero  \n",
    "Recall that attempting to divide by `0` results in a `ZeroDivisionError`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "10 / 0 "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Interpreter **raises an exception** of type `ZeroDivisionError`\n",
    "* Exception in IPython\n",
    "    * terminates the snippet, \n",
    "    * displays the exception’s traceback, then \n",
    "    * shows the next `In []` prompt so you can input the next snippet\n",
    "* Exception in a script terminates it and IPython displays the traceback"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Invalid Input \n",
    "* `int` raises a `ValueError` if you attempt to convert to an integer a string (like `'hello'`) that does not represent a number"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "value = int(input('Enter an integer: '))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------               "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `try` Statements\n",
    "* Can _handle_ exceptions so code can continue processing\n",
    "* Following code uses exception handling to catch and handle (i.e., deal with) any `ZeroDivisionError`s and `ValueError`s that arise—in this case, allowing the user to re-enter the input"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# dividebyzero.py\n",
    "\"\"\"Simple exception handling example.\"\"\"\n",
    "\n",
    "while True:\n",
    "    # attempt to convert and divide values\n",
    "    try:\n",
    "        number1 = int(input('Enter numerator: '))\n",
    "        number2 = int(input('Enter denominator: '))\n",
    "        result = number1 / number2\n",
    "    except ValueError:  # tried to convert non-numeric value to int\n",
    "        print('You must enter two integers\\n')\n",
    "    except ZeroDivisionError:  # denominator was 0\n",
    "        print('Attempted to divide by zero\\n')\n",
    "    else:  # executes only if no exceptions occur\n",
    "        print(f'{number1:.3f} / {number2:.3f} = {result:.3f}')\n",
    "        break  # terminate the loop"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `try` Clause\n",
    "* **`try` statements** enable exception handling\n",
    "* **`try` clause** followed by a suite of statements that _might_ raise exceptions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `except` Clause\n",
    "* `try` clause’s suite may be followed by one or more **`except` clauses** \n",
    "* Known as _exception handlers_\n",
    "* Each specifies the type of exception it handles"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `else` Clause\n",
    "* After the last `except` clause, an optional **`else` clause** specifies code that should execute only if the code in the `try` suite **did not raise exceptions**\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Flow of Control for a `ZeroDivisionError` \n",
    "* The point in the program at which an exception occurs is often referred to as the **raise point**\n",
    "* When an exception occurs in a `try` suite, it terminates immediately\n",
    "* If there are any `except` handlers following the `try` suite, program control transfers to the first one\n",
    "* If there are no `except` handlers, a process called _stack unwinding_ occurs (discussed later)\n",
    "* When an `except` clause successfully handles the exception, program execution resumes with the `finally` clause (if there is one), then with the next statement after the `try` statement."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Flow of Control for a `ValueError` \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Flow of Control for a Successful Division \n",
    "* When no exceptions occur in the `try` suite, program execution resumes with the `else` clause (if there is one); otherwise, program execution resumes with the next statement after the try statement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------             "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Catching Multiple Exceptions in One `except` Clause\n",
    "* If several `except` suites are identical, you can catch those exception types by specifying them as a tuple in a _single_ `except` handler:\n",
    "> ```python\n",
    "except (type1, type2, …) as variable_name:\n",
    "```\n",
    "* `as` clause is optional\n",
    "    * Typically, programs do not need to reference the caught exception object directly\n",
    "    * Can use the variable in the `as` clause to reference the exception object in the `except` suite"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What Exceptions Does a Function or Method Raise?\n",
    "* Before using any function or method, read its online API documentation\n",
    "    * Specifies what exceptions are thrown (if any) by the function or method \n",
    "    * Indicates reasons why such exceptions may occur\n",
    "* Next, read the online API documentation for each exception type to see potential reasons why such an exception occurs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What Code Should Be Placed in a `try` Suite?\n",
    "* For proper exception-handling granularity, each `try` statement should enclose a section of code small enough that, when an exception occurs, the specific context is known and the `except` handlers can process the exception properly\n",
    "* If many statements in a `try` suite raise the same exception types, multiple `try` statements may be required to determine each exception’s context"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------                "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# finally Clause\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The `finally` Clause of the `try` Statement\n",
    "* `try` statement may have a `finally` clause after any `except` clauses or the `else` clause\n",
    "* **`finally`** clause is guaranteed to execute\n",
    "    * In other languages, this makes the `finally` suite ideal for resource-deallocation code\n",
    "    * In Python, we prefer the `with` statement for this purpose"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    print('try suite with no exceptions raised')\n",
    "except:\n",
    "    print('this will not execute')\n",
    "else:\n",
    "    print('else executes because no exceptions in the try suite')\n",
    "finally:  \n",
    "    print('finally always executes')\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    print('try suite that raises an exception')\n",
    "    int('hello')\n",
    "    print('this will not execute')\n",
    "except ValueError:\n",
    "    print('a ValueError occurred')\n",
    "else:\n",
    "    print('else will not execute because an exception occurred')\n",
    "finally:  \n",
    "    print('finally always executes')\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Combining `with` Statements and `try…except` Statements \n",
    "* Most resources that require explicit release, such as files, network connections and database connections, have potential exceptions associated with processing those resources\n",
    "* _Robust_ file-processing code normally appears in a `try` suite containing a `with` statement to guarantee that the resource gets released"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "open('gradez.txt')  # non-existent file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    with open('gradez.txt', 'r') as accounts:\n",
    "        print(f'{\"ID\":<3}{\"Name\":<7}{\"Grade\"}')\n",
    "        for record in accounts:  \n",
    "            student_id, name, grade = record.split()\n",
    "            print(f'{student_id:<3}{name:<7}{grade}')\n",
    "except FileNotFoundError:\n",
    "    print('The file name you specified does not exist')\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------             "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Explicitly Raising an Exception\n",
    "* Sometimes you might need to write functions that raise exceptions to inform callers of errors that occur\n",
    "* **`raise`** statement explicitly raises an exception\n",
    "> ```python\n",
    "raise ExceptionClassName\n",
    "```\n",
    "* Creates an object of the specified exception class\n",
    "* Exception class name may be followed by parentheses containing arguments to initialize the exception object—typically a custom error message string\n",
    "* Code that raises an exception first should release any resources acquired before the exception occurred\n",
    "* It’s recommended that you use one of Python’s many [built-in exception types](https://docs.python.org/3/library/exceptions.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------              "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Stack Unwinding and Tracebacks\n",
    "* Each exception object stores information indicating the precise series of function calls that led to the exception\n",
    "* Helpful when debugging your code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def function1():\n",
    "    function2()\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def function2():\n",
    "    raise Exception('An exception occurred')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Calling `function1` results in the following traceback"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function1()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Traceback Details\n",
    "* Traceback shows the type of exception that occurred (`Exception`) followed by the complete function call stack that led to the raise point\n",
    "* The stack’s bottom function call is listed _first_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stack Unwinding\n",
    "* When an exception is _not_ caught in a given function, **stack unwinding** occurs\n",
    "* For an **uncaught exception**, IPython displays the traceback\n",
    "    * In interactive mode, IPython awaits your next input\n",
    "    * In a typical script, the script would terminate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tip for Reading Tracebacks\n",
    "* When reading a traceback, start from the end of the traceback and read the error message first\n",
    "* Then, read upward through the traceback, looking for the first line that indicates code you wrote in your program\n",
    "* Typically, this is the location in your code that led to the exception"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exceptions in `finally` Suites\n",
    "* Raising an exception in a `finally` suite can lead to subtle, hard-to-find problems\n",
    "* A `finally` suite should always enclose in a `try` statement any code that may raise an exception"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------        "
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
