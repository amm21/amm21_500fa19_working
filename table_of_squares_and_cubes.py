""" 
# this is the hardcoded version
print('number\t square\t cube')
print(0, '\t', 0 ** 2, '\t', 0 ** 3)
print(1, '\t', 1 ** 2, '\t', 1 ** 3)
print(2, '\t', 2 ** 2, '\t', 2 ** 3)
print(3, '\t', 3 ** 2, '\t', 3 ** 3)
print(4, '\t', 4 ** 2, '\t', 4 ** 3)
print(5, '\t', 5 ** 2, '\t', 5 ** 3)

print()
"""

"""
# this uses a loop and regular print
print('number\t square\t cube')
for i in range(6):
    print(i, '\t', i ** 2, '\t', i ** 3)
    
print()
"""

# this uses fprints and a loop to create the table
# use an fstring to re-create the table heading so that 
# 6 spaces are allocated for number,
# 8 spaces are allocated for square,
# 6 spaces are allocated for cube,
# and everything is right-aligned

# I put my +1 here instead of in the range because I thought it looked better
num_of_rows = int(input("Enter number of rows: ")) 
limit = num_of_rows + 1

print(f'{"number":>6}{"square":>8}{"cube":>6}')
for row in range(1, limit):
    print(f'{row:>6}{(row ** 2):>8}{(row ** 3):>6}')