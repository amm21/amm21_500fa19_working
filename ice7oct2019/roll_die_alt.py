# coding: utf-8
# anything starting with % in iPython is a "magic function/command"
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt # this is the conventionally used alias
import numpy as np
import random
import seaborn as sns 
rolls = [ random.randrange(1, 7) for i in range(600) ]
values, frequencies = np.unique( rolls, return_counts = True )
"""
Creating the Initial Bar Plot:

Let's create the bar plot's title, set its style, and 
then graph the die faces and frequencies
"""
# note that the following f-string will include
# the number of die rolls directly in the bar plot's
# title, and the use of a comma format specifier will
# display the number with thousands separators (so that
# a number like 60000 will be displayed as `60,000`)
title = f'Rolling a Six-Sided Die {len(rolls):,} Times' # the :, is the format specifier for thousands

# for other options, see: 
# https://seaborn.pydata.org/generated/seaborn.set_style.html
sns.set_style('whitegrid')


# create and display the bar plot
# see: https://seaborn.pydata.org/generated/seaborn.barplot.html
axes = sns.barplot( x=values, y=frequencies, palette='bright' )


"""
Setting the Window Title and Labeling the x- and y-Axes:

The next two statements add some descriptive text to
the bar plot...
"""
# set the title of the plot (set_title is a *method*
# of the axes object)
axes.set_title(title)

# label the axes
axes.set( xlabel='Die Value', ylabel='Frequency' )


"""
Finalizing the Bar Plot:

The next two statements complete the graph by making 
room for the text above each bar and then displaying it.
"""
# scale the y-axis (by 10%) to add room for text above bars
# (That is, the y-axis is 10% taller than whatever is the
#  height of the tallest plotted bar)
axes.set_ylim( top=max(frequencies) * 1.10 )

# create and display the text for each bar
# (The `patches` collection of the `axes` object contains 
#  2-D colored shapes that represent the plot's bars. 
#  The `for` statement uses `zip` to iterate through the 
#  `patches` and their corresponding `frequency` values.
#  Each iteration unpacks the tuple returned by `zip`
#  into `bar` and `frequency`. To see the official Matplotlib
#  documentation for the properties and methods of Rectangular `patches`, see:
#  https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.patches.Rectangle.html
for bar, frequency in zip(axes.patches, frequencies):
    text_x = ( bar.get_x() ) + ( bar.get_width() / 2.0 )
    text_y = bar.get_height() 
    text = f'{frequency:,}\n{frequency/len(rolls):.3%}'
    axes.text( text_x, text_y, text, 
               fontsize=11, ha='center', va='bottom' )
    # ha is horizontal align, va is vertical align
get_ipython().run_line_magic('recall', '6-7')
values, frequencies = np.unique( rolls, return_counts = True )
"""
Creating the Initial Bar Plot:

Let's create the bar plot's title, set its style, and 
then graph the die faces and frequencies
"""
# note that the following f-string will include
# the number of die rolls directly in the bar plot's
# title, and the use of a comma format specifier will
# display the number with thousands separators (so that
# a number like 60000 will be displayed as `60,000`)
title = f'Rolling a Six-Sided Die {len(rolls):,} Times' # the :, is the format specifier for thousands

# for other options, see: 
# https://seaborn.pydata.org/generated/seaborn.set_style.html
sns.set_style('whitegrid')


# create and display the bar plot
# see: https://seaborn.pydata.org/generated/seaborn.barplot.html
axes = sns.barplot( x=values, y=frequencies, palette='bright' )


"""
Setting the Window Title and Labeling the x- and y-Axes:

The next two statements add some descriptive text to
the bar plot...
"""
# set the title of the plot (set_title is a *method*
# of the axes object)
axes.set_title(title)

# label the axes
axes.set( xlabel='Die Value', ylabel='Frequency' )


"""
Finalizing the Bar Plot:

The next two statements complete the graph by making 
room for the text above each bar and then displaying it.
"""
# scale the y-axis (by 10%) to add room for text above bars
# (That is, the y-axis is 10% taller than whatever is the
#  height of the tallest plotted bar)
axes.set_ylim( top=max(frequencies) * 1.10 )

# create and display the text for each bar
# (The `patches` collection of the `axes` object contains 
#  2-D colored shapes that represent the plot's bars. 
#  The `for` statement uses `zip` to iterate through the 
#  `patches` and their corresponding `frequency` values.
#  Each iteration unpacks the tuple returned by `zip`
#  into `bar` and `frequency`. To see the official Matplotlib
#  documentation for the properties and methods of Rectangular `patches`, see:
#  https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.patches.Rectangle.html
for bar, frequency in zip(axes.patches, frequencies):
    text_x = ( bar.get_x() ) + ( bar.get_width() / 2.0 )
    text_y = bar.get_height() 
    text = f'{frequency:,}\n{frequency/len(rolls):.3%}'
    axes.text( text_x, text_y, text, 
               fontsize=11, ha='center', va='bottom' )
    # ha is horizontal align, va is vertical align
get_ipython().run_line_magic('recall', '12')
rolls = [ random.randrange(1, 7) for i in range(6000000) ]
get_ipython().run_line_magic('recall', '14')
values, frequencies = np.unique( rolls, return_counts = True )
"""
Creating the Initial Bar Plot:

Let's create the bar plot's title, set its style, and 
then graph the die faces and frequencies
"""
# note that the following f-string will include
# the number of die rolls directly in the bar plot's
# title, and the use of a comma format specifier will
# display the number with thousands separators (so that
# a number like 60000 will be displayed as `60,000`)
title = f'Rolling a Six-Sided Die {len(rolls):,} Times' # the :, is the format specifier for thousands

# for other options, see: 
# https://seaborn.pydata.org/generated/seaborn.set_style.html
sns.set_style('whitegrid')


# create and display the bar plot
# see: https://seaborn.pydata.org/generated/seaborn.barplot.html
axes = sns.barplot( x=values, y=frequencies, palette='bright' )


"""
Setting the Window Title and Labeling the x- and y-Axes:

The next two statements add some descriptive text to
the bar plot...
"""
# set the title of the plot (set_title is a *method*
# of the axes object)
axes.set_title(title)

# label the axes
axes.set( xlabel='Die Value', ylabel='Frequency' )


"""
Finalizing the Bar Plot:

The next two statements complete the graph by making 
room for the text above each bar and then displaying it.
"""
# scale the y-axis (by 10%) to add room for text above bars
# (That is, the y-axis is 10% taller than whatever is the
#  height of the tallest plotted bar)
axes.set_ylim( top=max(frequencies) * 1.10 )

# create and display the text for each bar
# (The `patches` collection of the `axes` object contains 
#  2-D colored shapes that represent the plot's bars. 
#  The `for` statement uses `zip` to iterate through the 
#  `patches` and their corresponding `frequency` values.
#  Each iteration unpacks the tuple returned by `zip`
#  into `bar` and `frequency`. To see the official Matplotlib
#  documentation for the properties and methods of Rectangular `patches`, see:
#  https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.patches.Rectangle.html
for bar, frequency in zip(axes.patches, frequencies):
    text_x = ( bar.get_x() ) + ( bar.get_width() / 2.0 )
    text_y = bar.get_height() 
    text = f'{frequency:,}\n{frequency/len(rolls):.3%}'
    axes.text( text_x, text_y, text, 
               fontsize=11, ha='center', va='bottom' )
    # ha is horizontal align, va is vertical align
