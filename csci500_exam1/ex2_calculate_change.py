"""
Problem 2 (script filename: ex2_calculate_change.py)

INSTRUCTIONS: Start by prompting the user for the price of an item that costs one dollar (100 cents) or less. Assuming the purchaser has paid with a dollar bill, your code should compute (and display) the amount of change, in cents, to be returned to the purchaser. Then, determine the disbursement of the change using the smallest number of pennies, nickels, dimes, and quarters necessary to return the appopriate change amount to the purchaser. 
"""

price = int(input('Enter a payment amount, in cents, up to but not greater than 100: '))

change = 100 - price
print(f'The total change returned will be {change} cents, disbursed as follows: ')

# Using integer (floor) division, we can calculate this with no loop.
quarters = change//25
dimes = (change%25)//10
nickels = (change%25%10)//5
pennies = change%25%10%5
print(f'Number of quarters: {quarters}\nNumber of dimes: {dimes}\nNumber of nickels: {nickels}\nNumber of pennies: {pennies}')








# If 14-18 is commented out and below is uncommented, this version also will work using a loop
"""
quarters = 0
dimes = 0
nickels = 0
pennies = 0

# can also use floor division and remainder to do this with no loop (thanks Dr. Canada)

# as long as there's still change to return, keep looping through and
# "adding" a coin to the "return pile"; each time you do this, reduce the
# amount of change that still needs to be returned
# this should always return the fewest amount of coins/most efficient combo 
# because it won't consider smaller coins unless the remaining change isn't 
# high enough for a larger coin
while change > 0:
     
    if change >= 25:
        quarters += 1
        change -= 25
    elif change >= 10:
        dimes += 1
        change -= 10
    elif change >= 5:
        nickels += 1
        change -= 5
    else:
        pennies += change
        change = 0
        
print(f'Number of quarters: {quarters}\nNumber of dimes: {dimes}\nNumber of nickels: {nickels}\nNumber of pennies: {pennies}')

"""