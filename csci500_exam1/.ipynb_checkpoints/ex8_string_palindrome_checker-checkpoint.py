"""
Problem 8 (script filename: ex8_string_palindrome_checker.py)

INSTRUCTIONS: We have previously written a script that will check to see if a 5-digit number is a palindrome (that is, it reads the same forwards and backwards). Strings can be palindromes as well, such as the word level or the phrase Taco cat, provided that we are ignoring spaces, or special characters, or case-sensitivity. In your script, write a function check_if_palindrome that takes any string as input and returns True if the string is a palindrome or False if the string is not a palindrome. In your function, create a stack with "push" and "pop" operations (simulated with a list, as you learned to do in Homework 3) to help you determine if the input string is a palindrome. Your function should ignore case sensitivity (i.e., lowercase letters will match their uppercase equivalents), and it should also ignore spaces and punctuation characters.

Your function should not produce any output itself. Instead, outside of the function, include a series of print statements that will generate the output for five different calls to your product function, as shown in the sample output below.

Hint: You already know about the string method lower that can be used to convert all of the uppercase characters of a string to their lowercase equivalents, but working with special characters is a different story. You haven't learned about regular expressions yet, but you can use them to help you ignore spaces and special characters for the purpose of this exercise. Here, I have provided a suggested starting point for the first 3 lines of your function definition:

def check_if_palindrome(string):
    # imports the regex (regular expressions) module
    import re 
    # performs a regex substitution to remove any non-alphanumeric characters
    string = re.sub('[^A-Za-z0-9]+', '', string) 

REMINDER: Apart from the regex subsitution code sample provided above, you may not use any programming techniques that we have not covered so far in this course. If you do so, you will not receive credit for this problem.
"""

def check_if_palindrome(string):
    # imports the regex (regular expressions) module
    import re 
    # performs a regex substitution to remove any non-alphanumeric characters
    string = re.sub('[^A-Za-z0-9]+', '', string) 
    
    # convert all to lowercase
    string = string.lower()
    
    stack_string = []
    # append to "push", pop to "pop"
    for letter in string:
        stack_string.append(letter)
        
    isPalindrome = True
    
    for letter in string:
        if letter != stack_string.pop():
            isPalindrome = False
            
    return isPalindrome



testing_string = ['uscb1234', 'madam', 'adam', 'Madam, I\'m Adam', 'A man, a plan, a canal: Panama']
for item in testing_string:
    print(f'Now checking \'{item}\':\n{check_if_palindrome(item)}\n')
