"""
Problem 6 (script filename: ex6_arbitrary_arguments.py)

INSTRUCTIONS: Compute the product of a series of integers that are passed to a function called product, which must receive and process a comma-separated series of integers (not a list or tuple) that is of arbitrary length — i.e., the function definition should use *args as its formal argument list. If the function is called with zero arguments (i.e., product()), then the function should return the value 0. Otherwise, function should return the product of all of the integer arguments that have been passed to it.

The function should not produce any output itself. Instead, outside of the function, include a series of print statements that will generate the output for five different calls to your product function, as shown in the sample output below.

REMINDER: Unless specified otherwise, you may not use any programming techniques that we have not covered so far in this course. If you do so, you will not receive credit for this problem.
"""

def product(*args):
    
    # len 0 would be no input arguments
    if len(args) == 0:
        return 0
    # this else works fine, isn't really needed
    # you could just remove the else and it would work the same
    else:
        final_product = 1

        for num in args:
            final_product *= num

        return final_product

print(f'Now calling product() -- i.e., with no arguments:\nResult: {product()}\n')

print(f'Now calling product(5):\nResult: {product(5)}\n')

print(f'Now calling product(5, 10):\nResult: {product(5,10)}\n')

print(f'Now calling product(20, 10, 5):\nResult: {product(20, 10, 5)}\n')

print(f'Now calling product(5, 4, 1, 2, 3):\nResult: {product(5, 4, 1, 2, 3)}\n')
