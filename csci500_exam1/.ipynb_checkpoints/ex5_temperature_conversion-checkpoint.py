"""
Problem 5 (script filename: ex5_temperature_conversion.py)

INSTRUCTIONS: Write a script that will display a table of Celsius temperatures (every 10 degrees, counting down from 100 to 0), and their Fahrenheit equivalents. Your script should include the definition of a function called convert_tc2tf that will accept a single Celsius temperature as its input argument and return the equivalent temperature in Fahrenheit degrees. For your tabulated output, use a single digit after the decimal point for the temperature in Fahrenheit degrees (as shown in the sample output below).

REMINDER: Unless specified otherwise, you may not use any programming techniques that we have not covered so far in this course. If you do so, you will not receive credit for this problem.

Also, if you misspell either of the words "Fahrenheit" or "Celsius" anywhere in your code or in your output (recognizing, of course, that your variable names should each be in all lowercase), you will get an automatic zero on this problem!
"""

def convert_tc2tf(temp_c):
    temp_f = (temp_c * (9/5)) + 32
    return temp_f

print(f'{"T: (Celsius)":<14}{"T: (Fahrenheit)":<14}')


# start at 100, go until reaching (not not including) -1 so that 0 is included,
# count down by 10
# calculate the temperature using the function with formula above and print it 
# properly formatted
for temp_c in range(100,-1,-10):
    temp_f = convert_tc2tf(temp_c)
    print(f'{temp_c:>12}{temp_f:>17.1f}') # always 1 digit after decimal and float
      
      
      
      
      
      
      
      
      
      
      
# this commented one ALSO WORKS, but I made one below that uses the range as mentioned in class (thanks Dr. Canada)
# temp_c = 100
# for row in range(11):
#     temp_f = convert_tc2tf(temp_c)
#     print(f'{temp_c:>12}{temp_f:>17.1f}') # always 1 digit after decimal and float
#     temp_c -= 10