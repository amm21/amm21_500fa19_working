"""
Problem 4 (script filename: ex4_three_largest_numbers_slicing.py)

INSTRUCTIONS: As above, you will write a script that will find and display the three largest values of 10 integers entered by the user via the keyboard. However, for this version of the program (which you may think of as a refactoring of the script you wrote for problem 3), you should store the entered numbers in a list, and you should use a combination of list slicing and the built-in max function.

REMINDER: Unless specified otherwise, you may not use any programming techniques that we have not covered so far in this course. If you do so, you will not receive credit for this problem.
"""

num_integers = 10
input_list = []

print(f'This program reads in {num_integers} integers from user input and displays the three largest values entered.\n')
    
for i in range(num_integers):
    user_input = input('Enter integer #' + str(i+1) + ' of ' + str(num_integers) + ': ')
    input_list.append(int(user_input))
    
# if using a simple for loop, only need to loop 3 times (thanks Dr. Canada)


highest_three = [] # create a list to append highest values to in the loop

# this loop will execute three times, and each time the "current" highest value
# is removed for the next iteration
# this results in the highest three values being collected
for i in range(3):
    
    highest = max(input_list) # get the highest value
    highest_index = input_list.index(highest) # get the highest value's index
    highest_three.append(highest) # append the highest value to a new list
    list_p1 = input_list[0:highest_index] # splice the original up to & not including current highest
    list_p2 = input_list[highest_index+1:] # splice the original after & not including current highest
    input_list = list_p1 + list_p2 # re-create the list without the current highest number
    
    
# because the list always has three elements and the highest numbers were always appended then removed before
# the process continued, they will always be in the correct order (first, second, third) to access them by index here
print(f'\nResults:\n1st largest: {highest_three[0]}\n2nd largest: {highest_three[1]}\n3rd largest: {highest_three[2]}')









# ignore this bit, doesn't work quite right and the final version above is much better
# this was just from the draft and you asked that we leave the older code
# first = max(input_list)
# for i in range(len(input_list)):
#     if input_list[i] == first:
#         list_p1 = input_list[0:i]
#         list_p2 = input_list[i+1:]
#         input_list = list_p1 + list_p2

# second = max(input_list)
# for i in range(len(input_list)):
#     if input_list[i] == second:
#         list_p1 = input_list[0:i]
#         list_p2 = input_list[i+1:]
#         input_list = list_p1 + list_p2

# third = max(input_list)
# for i in range(len(input_list)):
#     if input_list[i] == third:
#         list_p1 = input_list[0:i]
#         list_p2 = input_list[i+1:]
#         input_list = list_p1 + list_p2
