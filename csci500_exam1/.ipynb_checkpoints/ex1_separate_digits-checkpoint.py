"""
Problem 1 (script filename: ex1_separate_digits.py)

INSTRUCTIONS: Start by prompting the user to enter a positive integer that is between 2 and 10 digits long. Separate the number into its individual digits, and then append each individual digit to a list named digit_list (and then use that list object's append method). After all separated digits have been added to the list, your code should print the list to the screen.

Note #1: You may not use "string slicing" or string methods for separating the digits. Your code may only use floor division (//) and the remainder operator (%) to extract the individual digits. The only string operation you are permitted to use is the len function, which you can use to get the number of digits in the original integer entered by the user.

Note #2: Your code will likely work for an integer of any number of digits — I have arbitrarily chosen limits of 2 and 10 for the purpose of preventing the user from wasting time entering absurdly large values. You do not need to validate the user's entry to ensure an integer of appropriate length. Rather, for the sake of simplicity, assume the user actually follows the given instructions (which, admittedly, is not a great assumption). 
"""

number = int(input('Enter a positive integer that has between 2 and 10 digits: '))
            
num_digits = len(str(number)) # get length of input
digit_list = []

# this allows the script to work backwards, starting at the maximum divisor to extract
# the first digit and work towards the last
# if you start the divisor at 1 and go up, you would get the number backwards
mod = 1; 
divisor = 10 ** (num_digits - 1)


for i in range(num_digits):
    
    # first digit doesn't use mod
    if i == 0:
        digit = number // divisor
    # all other digits, get the mod before dividing
    else:
        digit = (number % mod) // divisor
    
    # update mod to be the old divisor, then update divisor by dividing by 10 to get digit in next place
    mod = divisor
    divisor /= 10 
    
    # use int to match the format of sample solutions, without int it will still
    # work but the list will contain doubles (in iPython at least, where this was tested)
    # decided to use int() because the data type was specifically mentioned in instructions
    # and the examples showed integers
    digit_list.append(int(digit))

print(digit_list)
