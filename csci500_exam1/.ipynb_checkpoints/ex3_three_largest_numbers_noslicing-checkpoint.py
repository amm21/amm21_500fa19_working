"""
Problem 3 (script filename: ex3_three_largest_numbers_noslicing.py)

INSTRUCTIONS: Using a loop and a nested selection statement, write a script that will find and display the three largest values of 10 integers entered by the user via the keyboard. If you elect to store the numbers as they are entered, please store them in a list. However, you are not permitted to use slicing or any list methods except for the append method. You are also not permitted to use lambdas, nor are you permitted to use any of Python's built-in reduction functions (such as max).

REMINDER: Unless specified otherwise, you may not use any programming techniques that we have not covered so far in this course. If you do so, you will not receive credit for this problem.
"""

num_integers = 10
input_list = []

print(f'This program reads in {num_integers} integers from user input and displays the three largest values entered.\n')
    
for i in range(num_integers):
    user_input = input('Enter integer #' + str(i+1) + ' of ' + str(num_integers) + ': ')
    input_list.append(int(user_input))

    
# get the first, then get the second, then get the third
# it would still work if you assigned 1st/2nd/3rd to index 0, I just
# chose the first three as placeholders; all numbers are still evaluated
first = input_list[0]
for num in input_list:
    if num > first:
        first = num

second = input_list[1]
for num in input_list:
    if num >= second and num < first:
        second = num

third = input_list[2]
for num in input_list:
    if num >= third and num < second:
        third = num

        
        
        
# when you check/update, rerank the top 3 (thanks Dr. Canada)
# first = input_list[0]
# second = input_list[1]
# third = input_list[2]
# for num in input_list:

#     if num >= first:
#         holder = first
#         first = num
#         second = holder  
#     elif num >= second:
#         holder = second
#         second = num
#         third = holder
#     elif num >= third:
#         third = num


        
print(f'\nResults:\n1st largest: {first}\n2nd largest: {second}\n3rd largest: {third}')
