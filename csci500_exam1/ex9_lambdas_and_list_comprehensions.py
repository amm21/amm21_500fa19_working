"""
Problem 9 (script filename: ex9_lambdas_and_list_comprehensions.py)

INSTRUCTIONS: Starting with a list of integers from 0 to 20 inclusively, use a lambda expression with filter (along with the map and sum functions) to calculate the total of the cubes of all odd integers between 0 and 20, and then display the result. Then, perform the same computation using a list comprehension (along with the sum function), and then display the result.

Hint #1: In lieu of a sample output for this problem, you should perform a computation outside of Python (say, using Microsoft Excel) to make sure you know what the intended result of each of the above computations should be. (You don't have to submit this manual computation.)

Hint #2: To help save you some time, you can use the following code as a starting point:

# ex9_lambdas_and_list_comprehensions.py
# Compute and display the total of the cubes of all odd integers between 0 and 20
# Start with a list of integers from 0 through 20 inclusively:
numbers = list(range(21))

# first, use filter, map, and sum to compute the 
# sum of the cubes of all odd integers between 0 and 20:
result1 = # replace this comment with the appropriate expression
print("Result1 =", result1)

# next, use a list comprehension (with the sum function) to compute 
# the sum of the cubes of all odd integers between 0 and 20:
result2 = # replace this comment with the appropriate expression
print("Result2 =", result2)

REMINDER: Unless specified otherwise, you may not use any programming techniques that we have not covered so far in this course. If you do so, you will not receive credit for this problem.
"""

# ex9_lambdas_and_list_comprehensions.py
"""Compute and display the total of the cubes of all odd integers between 0 and 20"""
# Start with a list of integers from 0 through 20 inclusively:
numbers = list(range(21))

# first, use filter, map, and sum to compute the 
# sum of the cubes of all odd integers between 0 and 20:
result1 = sum(map(lambda x: x**3, filter(lambda x: x % 2 != 0, numbers)))
print("Result1 = ", result1)

# next, use a list comprehension (with the sum function) to compute 
# the sum of the cubes of all odd integers between 0 and 20:
result2 = sum([num ** 3 for num in numbers if num % 2 != 0])
print("Result2 = ", result2)
