"""
Problem 7 (script filename: ex7_working_with_tuples.py)

INSTRUCTIONS: Define a function named shift that will receive four values as arguments (the values can be of any data type we have studied so far) and will return a tuple whose values are positionally shifted from their original order. More specifically, the first argument will be at index position 1 of the tuple, the second argument will be at index position 2, the third argument will be at index position 3, and the fourth argument will be at index position 0.

Outside of the function, the main part of your program should define variables w, x, y, and z to which the values 3.14159, 'USCB', True, and 1795 are respectively assigned. Then, call the function four times with the same argument list (w, x, y, z). Each each function call, the returned tuple should be unpacked into w, x, y, and z (in that order), and the new values of these variables should be displayed using formatted printing, as shown in the sample output below.

REMINDER: Unless specified otherwise, you may not use any programming techniques that we have not covered so far in this course. If you do so, you will not receive credit for this problem.
"""
w = 3.14159
x = 'USCB'
y = True
z = 1795

def shift(w, x, y, z):

    return(z, w, x, y)
    

# unpack/reassign and call, then print results each time
w, x, y, z = shift(w, x, y, z)
print(f'w: {w}; x: {x}; y: {y}; z: {z}')
w, x, y, z = shift(w, x, y, z)
print(f'w: {w}; x: {x}; y: {y}; z: {z}')
w, x, y, z = shift(w, x, y, z)
print(f'w: {w}; x: {x}; y: {y}; z: {z}')
w, x, y, z = shift(w, x, y, z)
print(f'w: {w}; x: {x}; y: {y}; z: {z}')