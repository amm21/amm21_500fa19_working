"""
Problem 10 (script filename: ex10_cctld_url_finder.py)

INSTRUCTIONS: In the parlance of the Internet, domain name extensions such as ".com" and ".net" are formally known as generic top-level domains, or gTLDs for short.

    Learn more here: https://en.wikipedia.org/wiki/Generic_top-level_domain

You may be familiar with websites like GoDaddy.com, which will try to find available domain names for you based on a name or phrase that you enter by appending all known gTLDs to the name you enter. (For example, if you were to search for "uscbeaufort", GoDaddy would look for domains like "uscbeaufort.com", "uscbeaufort.net", and so forth.)

    Here's an example search: https://www.godaddy.com/domainsearch/find?checkAvail=1&tmskey=&domainToCheck=uscbeaufort

Another domain name search tool, called Domainr (https://domainr.com/), works a little differently in that it will not only search for domains by appending top-level domains to your search string, but it will also parse the search string to see if any substrings match its database of top-level domains, including those that correspond to two-letter country codes (these are known as country code top level domains, or ccTLDs for short). If that's the case, then the search results will include domains in which the TLD is part of the search string, and in some cases, it will propose complete URLs that include both a short domain name as well as a subfolder name that, too, is part of the original search string. For example, if you search for "uscbeaufort", one of the proposed URLs is "uscbeau.fo/rt" (which includes the domain name "uscbeau.fo", which uses the ".fo" ccTLD, as well as the subfolder name "rt", which includes all characters in the search string that come after the ccTLD that it found as a substring).

    Here's an example search: https://domainr.com/?q=uscbeaufort

For this exercise, you will attempt to "reverse-engineer" the part of the algorithm that Domainr uses to identify domains and URLs that include country-code top level domains (ccTLDs). Your script should start by defining a list of all ccTLDs (as separate two-letter strings only), and then it should prompt the user to enter a search string. Your code should convert the search string to all lowercase characters, and then it should use regex substitution to remove all spaces and non-alphanumeric characters. Once the string is converted, your code should include a loop that uses string slicing to extract the last two characters of the string, and then it check to see if those two letters match any of the ccTLDs in the list you defined previously. If it does, it will split the string up to produce a domain name starting with all of the non-extracted characters, followed by a dot, and then ending with the matching ccTLD. The resulting domain name will then be printed to the screen.

Each subsequent iteration of the loop will "shift" (by one character to the left) the position at which it extracts a two-character substring. As above, it will check to see if the extracted two-letter substring matches any of the ccTLDs in your list. If it does, it will slice the string into 3 substrings to produce a URL starting with all of the non-extracted characters up to but not including the matching two-letter substring, followed by a dot, followed the matching ccTLD, followed by a forward slash, and then ending with all of the remaining characters in the string.

So, for example, if you entered "Video Game Name" as your search string:

    Spaces and special characters are removed, and the string is converted to all lowercase, producing the converted string videogamename that will be used in the loop
    The first iteration of the loop will extract the final two characters, me. This string matches the me ccTLD, so it will produce the domain name videogamena.me (this is a real URL, by the way).
    The next iteration of the loop will shift the extraction point to the left by one character to extract am, which is also a match. The resulting URL is videogamen.am/e
    The next iteration of the loop will shift the extraction point to the left by one character to extract na, which is also a match. The resulting URL is videogame.na/me
    The next iteration of the loop will shift the extraction point to the left by one character to extract en, which is not a match, so no URL will be produced or displayed for this iteration.

The loop will continue until it extracts the two-character string that follows the first letter of your string. (In this example, it extracts the id that follows the initial v. Since id is a match, the resulting URL will be v.id/eogamename).

You will need to do some research on your own to find a complete and up-to-date set of ccTLDs, perhaps in the form of an HTML table, which you will then use to prepare the corresponding list as used in your script. (I personally used Excel to make this process a little easier to deal with. I can't stop you from preparing your list manually, though this is potentially error-prone and can be rather time-consuming.)

You are welcome to use any Python techniques that we have used so far in the course, and you are also welcome to use the Regex substitution sample code provided in Problem 8 above. But as a general reminder, you may not use any programming techniques that we have not covered so far in this course. If you do so, you will not receive credit for this problem.
"""
# imports the regex (regular expressions) module
import re 

# complete list https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains#Country_code_top-level_domains
# using "" instead of '' because single quotes don't show up or copy/paste from Excel
domain_list = [ "ac", "ad", "ae", "af", "ag", "ai", "al", "am", "ao", "aq", "ar", "as", "at", "au", "aw", "ax", "az", "ba",
"bb", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bm", "bn", "bo", "br", "bs", "bt", "bw", "by", "bz", "ca", "cc", "cd", "cf",
"cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "cr", "cu", "cv", "cw", "cx", "cy", "cz", "de", "dj", "dk", "dm", "do", "dz",
"ec", "ee", "eg", "er", "es", "et", "eu", "fi", "fj", "fk", "fm", "fo", "fr", "ga", "gd", "ge", "gf", "gg", "gh", "gi", "gl",
"gm", "gn", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk", "hm", "hn", "hr", "ht", "hu", "id", "ie", "il", "im", "in",
"io", "iq", "ir", "is", "it", "je", "jm", "jo", "jp", "ke", "kg", "kh", "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la",
"lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly", "ma", "mc", "md", "me", "mg", "mh", "mk", "ml", "mm", "mn", "mo",
"mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my", "mz", "na", "nc", "ne", "nf", "ng", "ni", "nl", "no", "np", "nr",
"nu", "nz", "om", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm", "pn", "pr", "ps", "pt", "pw", "py", "qa", "re", "ro", "rs",
"ru", "rw", "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sk", "sl", "sm", "sn", "so", "sr", "ss", "st", "su", "sv", "sx",
"sy", "sz", "tc", "td", "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tr", "tt", "tv", "tw", "tz", "ua", "ug", "uk",
"us", "uy", "uz", "va", "vc", "ve", "vg", "vi", "vn", "vu", "wf", "ws", "ye", "yt", "za", "zm", "zw" ]


domain_len = len(domain_list)

# get and clean the input phrase
phrase = input('Enter a phrase, and we\'ll try to find some URLs that match: ')

# clean up the input phrase
phrase = re.sub('[^A-Za-z0-9]+', '', phrase) 
phrase = phrase.lower()
phrase_len = len(phrase)


# for loop, similar to the palindrome one and compare every two letters in the input to every element in
# the domain list
j = 0
for j in range(domain_len):  
    for i in range(1, phrase_len-1): # need at least one letter before domain
        
        # compare two letters to see if they match current domain
        if (phrase[i] + phrase[i+1]) == domain_list[j]: 
            name = phrase[0:i] # split all elements before match into "name"
            domain = domain_list[j] # use the current domain as "domain"
            page = phrase[i+2:phrase_len] # all elements after domain are "page"
            print(f'{name}.{domain}/{page}') # assemble using concatenation and format printing
            i += 1 
            
    j += 1


    
    
    
    
    
# for this to work it needs to work with the index and update that way (thanks Dr. Canada)
# I removed it because I ended up not using it, but leaving for future reference
# note that instead of domain, you would need to iterate through range of the length and
# update the list at that index with the regex
# for domain in domain_list:
#     domain = re.sub('[^A-Za-z0-9]+', '', domain)