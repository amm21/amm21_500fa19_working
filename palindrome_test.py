"""
This script tests to see if a 5-digit number entered
by the user reads the same both forwards and backwards
"""

number = int(input("Enter a 5-digit number: "))

digit1 = number // 10000
digit2 = (number % 10000) // 1000
digit4 = (number % 100) // 10
digit5 = (number % 10) // 1

if (digit1 == digit5) and (digit2 == digit4):
    print(f'{number} is a palindrome')
else:
    print(f'{number} is not a palindrome')