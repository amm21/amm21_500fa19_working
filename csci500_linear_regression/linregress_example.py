# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 16:54:11 2019

@author: amm21
"""

import matplotlib.pyplot as plt
from scipy import stats
import numpy as np

# Generate some data:
np.random.seed(12345678)
x = np.random.random(10)
y = 1.6*x + np.random.random(10)

# Perform the linear regression:
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
print("slope: %f    intercept: %f" % (slope, intercept))

# To get coefficient of determination (R-squared):
print("R-squared: %f" % r_value**2)

# Plot the data along with the fitted line:
plt.plot(x, y, 'o', label='original data')
plt.plot(x, intercept + slope*x, 'r', label='fitted line')
plt.legend()
plt.show()
