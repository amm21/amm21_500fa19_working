# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 16:54:11 2019

@author: amm21
"""

import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import pandas as pd
import re

def get_formatted_date( date_as_string ):
	result = re.fullmatch( r'(\d{4})-(\d{2})-(\d{2})', date_as_string )
	return int( ''.join( result.groups() ) ) if result else date_as_string
	
# Load some data:
xom_stockprice = pd.read_csv('https://www.quandl.com/api/v3/datasets/EOD/XOM.csv?api_key=hJxsUAzpE1Nws2isNxxX')
#x = xom_stockprice.Date 
# the above doesn't work because the date is in 'YYYY-MM-DD' format
# and is taken in as a string

#x = xom_stockprice.Date.map(get_formatted_date)
# the above has a "logical issue": when interpreting the dates as
# numbers, it starts the year at 20130101 and ends at 20139999 which
# does not follow the actual date-based data
# this results in large gaps where those missing "days" would be 

# as a work around for this, we can just number it  instead of using 
# dates to get a useful graph 
# we go backwards because the actual csv is newest-oldest data
x = pd.Series(range(len(xom_stockprice)-1,-1,-1))

y = xom_stockprice.Close

# Perform the linear regression:
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
print("slope: %f    intercept: %f" % (slope, intercept))

# To get coefficient of determination (R-squared):
print("R-squared: %f" % r_value**2)

# Plot the data along with the fitted line:
plt.plot(x, y, 'o', label='original data')
plt.plot(x, intercept + slope*x, 'r', label='fitted line')
plt.legend()
plt.show()
