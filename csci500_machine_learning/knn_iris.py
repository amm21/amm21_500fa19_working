"""
Repeat the KNN analysis for the Iris dataset

When running from an interactive IPython console,
don't forget to use the %matplotlib magic so that 
figures will be drawn (by default, in a new window)
"""
import sys
import numpy as np
# Read in the dataset and store in a 'Bunch' object
from sklearn.datasets import load_iris
iris = load_iris()  

# Checking the Sample and Target Sizes
print( "# of samples and features for each sample: " )
print( iris.data.shape )
print( "# of targets (labels); should match # of samples: " )
print( iris.target.shape )

# Splitting the Data for Training and Testing
from sklearn.model_selection import train_test_split
# by default, it splits 75 25
X_train, X_test, y_train, y_test = train_test_split(
    iris.data, iris.target, random_state=11)  # random_state for reproducibility

# NOTE: X is a matrix containing vectors (rows) of features
#       for each data sample. y is a vector that contains 
#       the expected classes (the "ground truth") for each data sample.


# Training and Testing Set Sizes
print( "# of samples (and features for each sample) in training partition: " ) 
print( X_train.shape )
print( "# of samples (and features for each sample) in testing partition:  " ) 
print( X_test.shape )
print() # blank line for readability

# Creating the Model
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier()

# Training the Model
# (Note: unlike most other supervised classification methods,
#        there's really no "model" in kNN -- we're basically 
#        just setting aside the training data, and the actual
#        computations will be performed in the prediction step)
knn.fit(X=X_train, y=y_train)

# Predicting Iris Classes
predicted = knn.predict(X=X_test) # classification based on training
expected = y_test # true classification based on actual data



print( "Predicted" )
print( "---------" )
print( predicted )
print() # blank line for readability

print( "Expected" )
print( "--------" )
print( expected )
print() # blank line for readability

# sys.exit() # didn't work
# raise SystemExit(0) # breaks out of a program early

wrong = [(p, e) for (p, e) in zip(predicted, expected) if p != e]

print( "Number of incorrect identifications")
print( "-----------------------------------")
print( wrong )
print() # blank line for readability

print("Classification accuracy (from explicit computation)")
print("---------------------------------------------------")
print(f'{(len(expected) - len(wrong)) / len(expected) :.2%}')
print() # blank line for readability

# Estimator Method score
print("Classification accuracy (using estimator method `score`)")
print("--------------------------------------------------------")
print(f'{knn.score(X_test, y_test):.2%}')
print() # blank line for readability

# Confusion Matrix
from sklearn.metrics import confusion_matrix
confusion = confusion_matrix(y_true=expected, y_pred=predicted)

print("Confusion matrix")
print("----------------")
print( confusion )
print() # blank line for readability

# Classification Report
from sklearn.metrics import classification_report

print("Classification report")
print("---------------------")
print( classification_report(expected, predicted, 
							 target_names=iris.target_names ) )
print() # blank line for readability

print("Classification report definitions:")
print("---------------------------------")
print("Precision, for a given class, is the fraction of ALL samples predicted")
print("  as belonging to that class that were CORRECTLY predicted as belonging to that class.")
print("  For example: of ALL samples that our classifier predicted as `Setosa`, ")
print("  how many of those did we get right?")      
print("  Precision is also known as the 'Positive Predictive Value', or PPV")
print("Recall, for a given class, tells you the fraction of samples ACTUALLY belonging")
print("  to that class that were CORRECLTY predicted as belonging to that class.")
print("  For example: of all `Setosa` samples in our testing dataset, ")
print("  how many did we correctly identify as such? That is, even if we got a bunch")
print("  of other predictions wrong, did we at least identify all of the 'Setosa' samples?")        
print("  Recall is also known as 'Sensitivity'.")
print() # blank line for readability

# Visualizing the Confusion Matrix
import pandas as pd
confusion_df = pd.DataFrame(confusion, index=iris.target_names, columns=iris.target_names)

# don't forget to execute the %matplotlib magic 
# when your kernel starts; otherwise the plots won't
# be generated when running the script from an 
# interactive IPython console      
import matplotlib.pyplot as plt
import seaborn as sns

plt.tight_layout() # improves layout of confusion matrix

axes = sns.heatmap( confusion_df, annot=True, cmap='nipy_spectral_r' )
axes.set_ylim(len(confusion_df), 0) # also improves layout
# note: on my LIB241 PC this line above isn't needed, only the tight_layout
# is. This one does nothing extra.

# show plot in window
plt.show()

# save figure in PNG (raster graphics) and EPS (vector graphics) image formats
plt.savefig('knn_iris_heatmap.png', format='png')
plt.savefig('knn_iris_heatmap.eps', format='eps')

# Perform K-Fold Cross-Validation (here, using 4 splits)
from sklearn.model_selection import KFold
kfold = KFold(n_splits=4, random_state=11, shuffle=True)

# Using the KFold Object with Function cross_val_score
from sklearn.model_selection import cross_val_score
scores = cross_val_score(estimator=knn, X=iris.data, y=iris.target, cv=kfold)

print("Cross-validation results:")
print("-------------------------")
print( scores )
print() # blank line for readability

print(f'Mean accuracy: {scores.mean():.2%}')
print() # blank line for readability

print(f'Accuracy standard deviation: {scores.std():.2%}')
print() # blank line for readability

# Perform Hyperparameter Tuning (which value of 'k' is best?)
# NOTE: the "k" in "kNN" has a different meaning than
#       the "k" in "k-fold cross-validation"!!"
print("Hyperparameter tuning results:")
print("------------------------------")
for k in range(1, 20, 2):  # k is an odd value 1-19; odds prevent ties
    kfold = KFold(n_splits=4, random_state=11, shuffle=True)
    knn = KNeighborsClassifier(n_neighbors=k)
    scores = cross_val_score(estimator=knn, 
        X=iris.data, y=iris.target, cv=kfold)
    print(f'k={k:<2}; mean accuracy={scores.mean():.2%}; ' +
          f'standard deviation={scores.std():.2%}')

    
print() # blank line for readability 

