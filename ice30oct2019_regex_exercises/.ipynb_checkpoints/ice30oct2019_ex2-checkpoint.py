"""
ice30oct2019_ex2.py
Write a short script that reads in a line of text (from user input) and then tokenizes the line (using the space character as a delimiter) — try doing this with a list comprehension if you can. The printed output of your script should include only those words beginning with the letter b (with each such word printed on its own line). 
"""
import re

phrase = input("Please enter a sentence or phrase: ")

# if you chain the .lower on the phrase split, it will return all lowercase
# if you chain the .lower on the token startswith, it will test properly and return the original case
# if you leave out .lower entirely, it will only check for starting with lowercase b
tokens = [ token for token in phrase.lower().split() if token.startswith('b') ]
# if you put print(token) above instead of just token, it will print as it goes

for token in tokens:
    print(token)
