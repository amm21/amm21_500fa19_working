"""
ice30oct2019_ex4.py
Use a regex to search through a string and locate all valid URLs. Assume that a valid URL starts with the protocol http:// or https:// and includes a domain name that follows the format www.domainname.extension, where extension must be two or more characters.

Use the following as your test string:

USCB's web address is https://www.uscb.edu and CNN's web address is http://www.cnn.com, but https://www.fakewebsite.c is not a valid URL
"""

import re

text = """USCB's web address is https://www.uscb.edu and CNN's web address is http://www.cnn.com, but https://www.fakewebsite.c is not a valid URL"""

# literal http, s if present or absent (s?), literal ://www, literal ., 1 or more word characters (\w+),
# literal ., 2 or more lowercase letters ([a-z]{2,})
urls = re.findall( r'https?://www\.\w+\.[a-z]{2,}' , text )
print(urls)