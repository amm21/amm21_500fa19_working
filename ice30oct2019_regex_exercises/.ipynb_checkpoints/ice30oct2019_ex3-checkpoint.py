"""
ice30oct2019_ex3.py
Re-implement the script you wrote for Exercise 2 above, but this time use the findall function of the re module to produce a list of words that match an appropriate regular expression. Then, output the matching words (with each word on its own line).
"""

import re

phrase = input("Please enter a sentence or phrase: ")

# https://regex101.com/r/CpdkB3/2 link you wanted
# findall takes two arguments, the regex and the phrase to search
# it produces a list
# the lower is not necessary because [Bb] but does result in them all printing
# out to the screen as lowercase versions
matches = [ match for match in re.findall(r'[Bb]\w*', phrase.lower()) ]
# if you use print(match) above instead of match, it will print as it goes

for match in matches:
    print(match)