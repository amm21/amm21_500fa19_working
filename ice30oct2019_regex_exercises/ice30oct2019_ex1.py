"""
ice30oct2019_ex1.py
Write a short script that uses a regex to check whether a sentence or phrase (entered via user input) contains more than one space between any two words. If so, use an appropriate method to remove the extra spaces and then print out the sentence with the extra spaces removed (but make sure that there's still a single space between any two words!)
"""
# https://regex101.com/ useful site

import re

phrase = input("Please enter a sentence or phrase with extra spaces between words: ")

# sub takes 3 arguments: 
# 1) the regex (r'')
# 2) the replacement text
# #) the string to be searched
new_phrase = re.sub(r'\s+', ' ', phrase)
print(new_phrase)
