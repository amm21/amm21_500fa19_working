"""
ice30oct2019_ex5.py
Write a script that reads in a standard 10-digit U.S. telephone number is ANY format (as long as there are ten digits) and then uses a regex with capture groups so that you can output a "munged" version of the telephone number with this specific format (for example): 
(800)555-1212
where in the example above, 800 represents the 3-digit area code, 555 is the 3-digit exchange, and 1212 is the 4-digit extension.
"""

import re

phone_num = input("Please enter a 10 digit phone number in any format: ")

# any character 0 or more times, next 3 digits (), repeat for 3 3 4, then any 
# character 0 or more times at the end just incase
pattern = '.*(\d{3}).*(\d{3}).*(\d{4}).*'
# note that the numbers per section do need to be together in the input string

result = re.search( pattern, phone_num )
# note that the sections are stored in tuples

# we're working more with "capture groups" than an index here
area_code = result.group(1) # analogous to \1 in a text editor or regex101 (we do not use index 0 here)
exchange = result.group(2) # \2
extension = result.group(3) # \3

print(f'({area_code}){exchange}-{extension}')

