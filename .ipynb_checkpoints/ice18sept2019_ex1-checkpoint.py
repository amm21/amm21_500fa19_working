# Write a script that displays the following output:
# 
# @@@@@@@
# @@@@@@@
#
# In your script, use the range function and nested loops.

number_of_rows = int(input('Choose the number of rows: '))
number_of_columns = int(input('Choose the number of columns: '))
    
for row in range(number_of_rows):
    print() # after printing the current line, advance to the next line
    for column in range(number_of_columns):
        print('@', end='')