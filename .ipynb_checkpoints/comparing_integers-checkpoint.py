"""
comparing_integers.py

Illustrates the use of if statements and comparison operators.
"""

print('Enter two integers and I will tell you', 
      'the relationships they satisfy.')

# read first integer
number1 = int(input('Enter first integer: '))

# read second integer
number2 = int(input('Enter second integer: '))

if number1 == number2:
    print(number1, 'is equal to', number2)

# TODO: Write an if-statement to test whether number1 is NOT EQUAL to number2
if number1 != number2:
    print(number1, 'is not equal to', number2)

"""
Note: Indentation matters! The indented block of code is sometimes called a `suite`.
The "Style Guide for Python Code" recommends four-space indents. Keep your 
indentation consistent! Also, Forgetting the colon (:) after the condition 
is a common syntax error in Python.
"""

if number1 < number2:
    print(number1, 'is less than', number2)

if number1 > number2:
    print(number1, 'is greater than', number2)

if number1 <= number2:
    print(number1, 'is less than or equal to', number2)

if number1 >= number2:
    print(number1, 'is greater than or equal to', number2)

"""
x = 1 + 2 + 3
    + 4 + 5 + 6

print(x)
"""    